﻿//
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Reflection;
using WbxmlDocument;
namespace TestHttpModule
{
    public class TestFilter : Stream
    {
        private Stream output;
        public TestFilter(Stream s)
        {
            output = s;
        }
        public override bool CanRead
        {
            get { return output.CanRead; }
        }

        public override bool CanSeek
        {
            get { return output.CanSeek; }
        }

        public override bool CanWrite
        {
            get { return output.CanWrite; }
        }

        public override void Flush()
        {
            output.Flush();
        }

        public override long Length
        {
            get { return output.Length; }
        }

        public override long Position
        {
            get { return output.Position; }
            set { output.Position = value; }
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            return output.Read(buffer, offset, count);
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            return output.Seek(offset, origin);
        }

        public override void SetLength(long value)
        {
            output.SetLength(value);
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            string logfile = @"c:\test.log";
            try
            {
                if (HttpContext.Current != null)
                {
                    output.Write(buffer, offset, count);
                    HttpContext context = HttpContext.Current;
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine("Raw URL:" + context.Request.RawUrl);
                    sb.AppendLine("Buffer Length:" + buffer.Length.ToString() + " offset:" + offset.ToString() + " count:" + count.ToString());
                    sb.AppendLine("Encoding:" + context.Response.ContentEncoding.ToString());
                    string result = System.Text.UTF8Encoding.UTF8.GetString(buffer, offset, count);
                    sb.AppendLine("Result:" + result);
                    sb.AppendLine("Bytes:");
                    sb.Append(BitConverter.ToString(buffer));
                    sb.AppendLine();
                    File.AppendAllText(logfile, sb.ToString() + Environment.NewLine);

                    MemoryStream ms = new MemoryStream(buffer);
                    ms.Position = 0;
                    WbxmlReader wr=new WbxmlReader(ms,true);
                    System.Xml.XmlDocument doc=  wr.ReadXmlDocument();
                    File.AppendAllText(logfile, doc.InnerXml);
                    File.AppendAllText(logfile, Environment.NewLine);

                    //OR use reflection to create the WbxmlReader from assembly Microsoft.Exchange.AirSync.dll
                    //string assembly = Path.Combine(@"C:\Program Files\Microsoft\Exchange Server\V15\Bin", "Microsoft.Exchange.AirSync.dll");
                    //Assembly a = Assembly.LoadFile(assembly);
                    //Type t = a.GetType("Microsoft.Exchange.AirSync.Wbxml.WbxmlReader");
                    //object o = Activator.CreateInstance(t,ms,true);
                    //if (o != null)
                    //{
                    //    File.AppendAllText(@"c:\1234.txt", "object created");
                    //    MethodInfo toxml = t.GetMethod("ReadXmlDocument");
                    //    MethodInfo su = t.GetMethod("SuppressDisposeTracker");
                    //    su.Invoke(o, null);
                    //    object result2 = toxml.Invoke(o, null);
                    //    if (result2 != null)
                    //    {
                    //        File.AppendAllText(@"c:\1234.txt", "result created");
                    //        File.AppendAllText(@"c:\1234.txt", Environment.NewLine);
                    //        System.Xml.XmlDocument doc = result2 as System.Xml.XmlDocument;
                    //        File.AppendAllText(@"c:\1234.txt", doc.InnerXml);
                    //        File.AppendAllText(@"c:\1234.txt", Environment.NewLine);
                    //    }
                    //    //System.Xml.XmlDocument doc=
                    //}
                }
            }
            catch (Exception ex)
            {
                File.AppendAllText(logfile, ex.ToString() + Environment.NewLine);
            }
            
        }
    }

    public class TestHttpModule : IHttpModule
    {
        #region IHttpModule Members

        public void Dispose()
        {
        }

        public void Init(HttpApplication context)
        {
            context.PreRequestHandlerExecute += (sender, e) =>
            {
                string logfile = @"c:\tets.log";
                try
                {
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine("Request RawUrl:" + context.Request.RawUrl);
                    sb.AppendLine("Encoding:" + context.Request.ContentEncoding.ToString());
                    byte[] bytes = context.Request.BinaryRead(context.Request.ContentLength);
                    sb.AppendLine(Encoding.UTF8.GetString(bytes));
                    File.AppendAllText(logfile, sb.ToString());
                    WbxmlReader wr = new WbxmlReader(context.Request.InputStream, true);
                    System.Xml.XmlDocument doc = wr.ReadXmlDocument();
                    File.AppendAllText(logfile, doc.InnerXml);
                    File.AppendAllText(logfile, Environment.NewLine);
                    
                }
                catch (Exception ex)
                {
                    File.AppendAllText(logfile, ex.ToString());
                }
                

            };
            context.BeginRequest += (sender, e) =>
            {
                
                context.Response.Filter =
                    new TestFilter(context.Response.Filter);
            };
            
        }

        #endregion
    }
}