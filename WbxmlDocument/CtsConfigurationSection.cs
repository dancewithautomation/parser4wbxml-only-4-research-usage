﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Xml;

namespace WbxmlDocument
{
    internal sealed class CtsConfigurationSection : ConfigurationSection
    {
        // Fields
        private static ConfigurationPropertyCollection properties;
        private Dictionary<string, IList<CtsConfigurationSetting>> subSections = new Dictionary<string, IList<CtsConfigurationSetting>>();

        // Methods
        protected override void DeserializeSection(XmlReader reader)
        {
            IList<CtsConfigurationSetting> list = new List<CtsConfigurationSetting>();
            this.subSections.Add(string.Empty, list);
            if (!reader.Read() || (reader.NodeType != XmlNodeType.Element))
            {
                throw new ConfigurationErrorsException("error", reader);
            }
            if (!reader.IsEmptyElement)
            {
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.Element)
                    {
                        if (reader.IsEmptyElement)
                        {
                            CtsConfigurationSetting item = this.DeserializeSetting(reader);
                            list.Add(item);
                        }
                        else
                        {
                            IList<CtsConfigurationSetting> list2;
                            string name = reader.Name;
                            if (!this.subSections.TryGetValue(name, out list2))
                            {
                                list2 = new List<CtsConfigurationSetting>();
                                this.subSections.Add(name, list2);
                            }
                            while (reader.Read())
                            {
                                if (reader.NodeType == XmlNodeType.Element)
                                {
                                    if (!reader.IsEmptyElement)
                                    {
                                        throw new ConfigurationErrorsException("error", reader);
                                    }
                                    CtsConfigurationSetting setting2 = this.DeserializeSetting(reader);
                                    list2.Add(setting2);
                                }
                                else
                                {
                                    if (reader.NodeType == XmlNodeType.EndElement)
                                    {
                                        continue;
                                    }
                                    if ((reader.NodeType == XmlNodeType.CDATA) || (reader.NodeType == XmlNodeType.Text))
                                    {
                                        throw new ConfigurationErrorsException("error", reader);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (reader.NodeType == XmlNodeType.EndElement)
                        {
                            return;
                        }
                        if ((reader.NodeType == XmlNodeType.CDATA) || (reader.NodeType == XmlNodeType.Text))
                        {
                            throw new ConfigurationErrorsException("error", reader);
                        }
                    }
                }
            }
        }

        private CtsConfigurationSetting DeserializeSetting(XmlReader reader)
        {
            CtsConfigurationSetting setting = new CtsConfigurationSetting(reader.Name);
            if (reader.AttributeCount > 0)
            {
                while (reader.MoveToNextAttribute())
                {
                    string name = reader.Name;
                    string str3 = reader.Value;
                    setting.AddArgument(name, str3);
                }
            }
            return setting;
        }

        // Properties
        protected override ConfigurationPropertyCollection Properties
        {
            get
            {
                if (properties == null)
                {
                    properties = new ConfigurationPropertyCollection();
                }
                return properties;
            }
        }

        public Dictionary<string, IList<CtsConfigurationSetting>> SubSectionsDictionary
        {
            get
            {
                return this.subSections;
            }
        }
    }

 

}
