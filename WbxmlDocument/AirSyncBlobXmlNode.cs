﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

namespace WbxmlDocument
{
    internal class AirSyncBlobXmlNode : XmlElement
    {
        private byte[] byteArray;
        private XmlNodeType originalNodeType;
        private System.IO.Stream stream;
        private long streamDataSize;

        public AirSyncBlobXmlNode(string prefix, string localName, string namespaceURI, XmlDocument doc)
            : base(prefix, localName, namespaceURI, doc)
        {
            this.streamDataSize = -1L;
            this.originalNodeType = XmlNodeType.CDATA;
        }

        public void CopyStream(System.IO.Stream outputStream)
        {
            if (this.stream != null)
            {
                if (this.stream.CanSeek)
                {
                    this.stream.Seek(0L, SeekOrigin.Begin);
                }
                else
                {
                    //TODO:
                }
                AirSyncStream stream = this.stream as AirSyncStream;
                if (stream != null)
                {
                    stream.CopyStream(outputStream, (int)this.StreamDataSize);
                }
                else
                {
                    StreamHelper.CopyStream(this.stream, outputStream, (int)this.StreamDataSize);
                }
            }
        }

        public override int GetHashCode()
        {
            AirSyncStream stream = this.stream as AirSyncStream;
            if (stream != null)
            {
                return stream.StreamHash;
            }
            if (this.byteArray != null)
            {
                return (int)StreamHelper.UpdateCrc32(0, this.byteArray, 0, this.byteArray.Length);
            }
            return base.GetHashCode();
        }

        public byte[] ByteArray
        {
            get
            {
                return this.byteArray;
            }
            set
            {
                if (this.stream != null)
                {
                    throw new InvalidOperationException("stream has already been assigned");
                }
                this.byteArray = value;
            }
        }

        public override string InnerText
        {
            get
            {
                return "*** blob ***";
            }
            set
            {
                throw new NotImplementedException("This kind of node should contain a blob, not a string");
            }
        }

        public XmlNodeType OriginalNodeType
        {
            get
            {
                return this.originalNodeType;
            }
            set
            {
                this.originalNodeType = value;
            }
        }

        public System.IO.Stream Stream
        {
            get
            {
                return this.stream;
            }
            set
            {
                if (this.byteArray != null)
                {
                    throw new InvalidOperationException("byteArray has already been assigned");
                }
                this.stream = value;
            }
        }

        public long StreamDataSize
        {
            get
            {
                if (this.streamDataSize < 0L)
                {
                    return this.Stream.Length;
                }
                return this.streamDataSize;
            }
            set
            {
                this.streamDataSize = value;
            }
        }
    }
}
