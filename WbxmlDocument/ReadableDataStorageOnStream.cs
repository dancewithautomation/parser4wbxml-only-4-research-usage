﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace WbxmlDocument
{
    internal class ReadableDataStorageOnStream : ReadableDataStorage
    {
        // Fields
        private bool ownsStream;
        private Stream stream;

        // Methods
        public ReadableDataStorageOnStream(Stream stream, bool ownsStream)
        {
            if (stream == null)
            {
                throw new ArgumentNullException("stream");
            }
            this.stream = stream;
            this.ownsStream = ownsStream;
        }

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing && this.ownsStream)
                {
                    this.stream.Dispose();
                }
                this.stream = null;
            }
            base.Dispose(disposing);
        }

        private int InternalRead(long position, byte[] buffer, int offset, int count)
        {
            this.stream.Position = position;
            return this.stream.Read(buffer, offset, count);
        }

        public override int Read(long position, byte[] buffer, int offset, int count)
        {
            base.ThrowIfDisposed();
            if (base.isReadOnly)
            {
                lock (base.readOnlyLock)
                {
                    return this.InternalRead(position, buffer, offset, count);
                }
            }
            return this.InternalRead(position, buffer, offset, count);
        }

        // Properties
        public override long Length
        {
            get
            {
                return this.stream.Length;
            }
        }
    }

 

}
