﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WbxmlDocument
{
    [Serializable]
    internal class WbxmlException : Exception
    {
        public WbxmlException(string message)
            : base(message)
        {
        }

        public WbxmlException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
