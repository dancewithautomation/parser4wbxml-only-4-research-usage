﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WbxmlDocument
{
    internal class CtsConfigurationArgument
    {
        // Fields
        private string name;
        private string value;

        // Methods
        internal CtsConfigurationArgument(string name, string value)
        {
            this.name = name;
            this.value = value;
        }

        // Properties
        public string Name
        {
            get
            {
                return this.name;
            }
        }

        public string Value
        {
            get
            {
                return this.value;
            }
        }
    }

 

}
