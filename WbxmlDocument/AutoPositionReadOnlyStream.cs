﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace WbxmlDocument
{
    internal sealed class AutoPositionReadOnlyStream : Stream, ICloneableStream
    {
        // Fields
        private long position;
        private ReadableDataStorage storage;

        // Methods
        private AutoPositionReadOnlyStream(AutoPositionReadOnlyStream original)
        {
            original.storage.AddRef();
            this.storage = original.storage;
            this.position = original.position;
        }

        public AutoPositionReadOnlyStream(Stream wrapped, bool ownsStream)
        {
            this.storage = new ReadableDataStorageOnStream(wrapped, ownsStream);
            this.position = wrapped.Position;
        }

        public Stream Clone()
        {
            if (this.storage == null)
            {
                throw new ObjectDisposedException("AutoPositionReadOnlyStream");
            }
            return new AutoPositionReadOnlyStream(this);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.storage != null))
            {
                this.storage.Release();
                this.storage = null;
            }
            base.Dispose(disposing);
        }

        public override void Flush()
        {
            throw new NotSupportedException();
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            if (this.storage == null)
            {
                throw new ObjectDisposedException("AutoPositionReadOnlyStream");
            }
            int num = this.storage.Read(this.position, buffer, offset, count);
            this.position += num;
            return num;
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            if (this.storage == null)
            {
                throw new ObjectDisposedException("AutoPositionReadOnlyStream");
            }
            switch (origin)
            {
                case SeekOrigin.Begin:
                    break;

                case SeekOrigin.Current:
                    offset += this.position;
                    break;

                case SeekOrigin.End:
                    offset += this.Length;
                    break;

                default:
                    throw new ArgumentException("origin");
            }
            if (0L > offset)
            {
                throw new ArgumentOutOfRangeException("offset", "CannotSeekBeforeBeginning");
            }
            this.position = offset;
            return this.position;
        }

        public override void SetLength(long value)
        {
            throw new NotSupportedException();
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            throw new NotSupportedException();
        }

        // Properties
        public override bool CanRead
        {
            get
            {
                return (this.storage != null);
            }
        }

        public override bool CanSeek
        {
            get
            {
                return (this.storage != null);
            }
        }

        public override bool CanWrite
        {
            get
            {
                return false;
            }
        }

        public override long Length
        {
            get
            {
                if (this.storage == null)
                {
                    throw new ObjectDisposedException("AutoPositionReadOnlyStream");
                }
                return this.storage.Length;
            }
        }

        public override long Position
        {
            get
            {
                if (this.storage == null)
                {
                    throw new ObjectDisposedException("AutoPositionReadOnlyStream");
                }
                return this.position;
            }
            set
            {
                if (this.storage == null)
                {
                    throw new ObjectDisposedException("AutoPositionReadOnlyStream");
                }
                if (value < 0L)
                {
                    throw new ArgumentOutOfRangeException("value", "CannotSeekBeforeBeginning");
                }
                this.position = value;
            }
        }
    }

 

}
