﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WbxmlDocument
{
    internal static class ApplicationServices
    {
        // Fields
        private static IApplicationServices provider = LoadServices();

        // Methods
        public static CtsConfigurationSetting GetSimpleConfigurationSetting(string subSectionName, string settingName)
        {
            CtsConfigurationSetting setting = null;
            foreach (CtsConfigurationSetting setting2 in Provider.GetConfiguration(subSectionName))
            {
                if (string.Equals(setting2.Name, settingName, StringComparison.OrdinalIgnoreCase))
                {
                    if (setting != null)
                    {
                        Provider.LogConfigurationErrorEvent();
                        return setting;
                    }
                    setting = setting2;
                }
            }
            return setting;
        }

        private static IApplicationServices LoadServices()
        {
            return new DefaultApplicationServices();
        }

        internal static int ParseIntegerSetting(CtsConfigurationSetting setting, int defaultValue, int min, bool kilobytes)
        {
            int num;
            if ((setting.Arguments.Count != 1) || !setting.Arguments[0].Name.Equals("Value", StringComparison.OrdinalIgnoreCase))
            {
                Provider.LogConfigurationErrorEvent();
                return defaultValue;
            }
            if (setting.Arguments[0].Value.Trim().Equals("unlimited", StringComparison.OrdinalIgnoreCase))
            {
                return 0x7fffffff;
            }
            if (!int.TryParse(setting.Arguments[0].Value.Trim(), out num))
            {
                Provider.LogConfigurationErrorEvent();
                return defaultValue;
            }
            if (num < min)
            {
                Provider.LogConfigurationErrorEvent();
                return defaultValue;
            }
            if (!kilobytes)
            {
                return num;
            }
            if (num > 0x1fffff)
            {
                return 0x7fffffff;
            }
            return (num * 0x400);
        }

        // Properties
        public static IApplicationServices Provider
        {
            get
            {
                return provider;
            }
        }
    }

 

}
