﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace WbxmlDocument
{
    public class BufferPool
    {
        private readonly int bufferSize;
        private readonly bool cleanBufferOnRelease;
        private readonly int countLimit;
        public const int DefaultMaxStackDepth = 20;
        private readonly bool enablePoolSharing;
        private static bool enableReleaseTracking;
        private readonly object initializationLock;
        public const int MaxBufferSize = 0x100000;
        public const int MinBufferSize = 0x400;
        private const int MinBufferSizeToUseSharing = 0x16800;
        private Stack<byte[]>[] pool;
        private Queue<byte[]> recentRelease;
        private Dictionary<byte[], StackTrace> recentReleaseStacks;

        public BufferPool(int bufferSize)
            : this(bufferSize, 20, true, false)
        {
        }

        public BufferPool(int bufferSize, bool cleanBufferOnRelease)
            : this(bufferSize, 20, cleanBufferOnRelease, false)
        {
        }

        public BufferPool(int bufferSize, int maxBufferCountPerProcessor)
            : this(bufferSize, maxBufferCountPerProcessor, true, false)
        {
        }

        public BufferPool(int bufferSize, int maxBufferCountPerProcessor, bool cleanBufferOnRelease)
            : this(bufferSize, maxBufferCountPerProcessor, cleanBufferOnRelease, false)
        {
        }

        public BufferPool(int bufferSize, int maxBufferCountPerProcessor, bool cleanBufferOnRelease, bool enablePoolSharing)
        {
            this.initializationLock = new object();
            this.pool = new Stack<byte[]>[Environment.ProcessorCount];
            if (bufferSize > 0x100000)
            {
                throw new ArgumentOutOfRangeException("bufferSize", string.Format(CultureInfo.InvariantCulture, "LargeBuffer", new object[] { bufferSize, 0x100000 }));
            }
            if (bufferSize < 0x400)
            {
                throw new ArgumentOutOfRangeException("bufferSize", string.Format(CultureInfo.InvariantCulture, "SmallBuffer", new object[] { bufferSize, 0x400 }));
            }
            if (maxBufferCountPerProcessor < 1)
            {
                throw new ArgumentOutOfRangeException("maxBufferCountPerProcessor", string.Format(CultureInfo.InvariantCulture, "InvalidMaxBufferCount", new object[0]));
            }
            this.countLimit = Math.Min(maxBufferCountPerProcessor, 0x400000 / bufferSize);
            this.bufferSize = bufferSize;
            this.cleanBufferOnRelease = cleanBufferOnRelease;
            this.enablePoolSharing = (bufferSize >= 0x16800) || enablePoolSharing;
        }

        public byte[] Acquire()
        {
            int currentProcessor = NativeMethods.CurrentProcessor;
            Stack<byte[]> pool = this.GetPool(currentProcessor);
            lock (pool)
            {
                if (pool.Count > 0)
                {
                    return pool.Pop();
                }
            }
            if (this.enablePoolSharing)
            {
                for (int i = 1; i < this.pool.Length; i++)
                {
                    pool = this.GetPool((currentProcessor + i) % this.pool.Length);
                    lock (pool)
                    {
                        if (pool.Count > 0)
                        {
                            return pool.Pop();
                        }
                    }
                }
            }
            return new byte[this.bufferSize];
        }

        private Stack<byte[]> GetPool(int index)
        {
            if (this.pool[index] == null)
            {
                lock (this.initializationLock)
                {
                    if (this.pool[index] == null)
                    {
                        this.pool[index] = new Stack<byte[]>(this.countLimit);
                    }
                }
            }
            return this.pool[index];
        }

        public void Release(byte[] buffer)
        {
            if (buffer == null)
            {
                throw new ArgumentNullException("buffer");
            }
            if (buffer.Length != this.bufferSize)
            {
                throw new ArgumentException("BufferMismatch", "buffer");
            }
            if (this.cleanBufferOnRelease)
            {
                Array.Clear(buffer, 0, this.bufferSize);
            }
            if (EnableReleaseTracking)
            {
                lock (this)
                {
                    if (this.recentRelease == null)
                    {
                        this.recentRelease = new Queue<byte[]>(0x400);
                    }
                    if (this.recentReleaseStacks == null)
                    {
                        this.recentReleaseStacks = new Dictionary<byte[], StackTrace>();
                    }
                    if (this.recentRelease.Contains(buffer))
                    {
                        throw new InvalidOperationException("Buffer is released twice! Originally released\n" + this.recentReleaseStacks[buffer].ToString() + "\nSecond release is\n" + new StackTrace(true).ToString());
                    }
                    this.recentRelease.Enqueue(buffer);
                    this.recentReleaseStacks.Add(buffer, new StackTrace(true));
                    if (this.recentRelease.Count > 0x3e8)
                    {
                        buffer = this.recentRelease.Dequeue();
                        this.recentReleaseStacks.Remove(buffer);
                    }
                    else
                    {
                        return;
                    }
                }
            }
            int currentProcessor = NativeMethods.CurrentProcessor;
            Stack<byte[]> pool = this.GetPool(currentProcessor);
            if (pool.Count < this.countLimit)
            {
                lock (pool)
                {
                    if (pool.Count < this.countLimit)
                    {
                        pool.Push(buffer);
                        buffer = null;
                    }
                }
            }
            if ((buffer != null) && this.enablePoolSharing)
            {
                for (int i = 1; i < this.pool.Length; i++)
                {
                    pool = this.GetPool((currentProcessor + i) % this.pool.Length);
                    lock (pool)
                    {
                        if (pool.Count < this.countLimit)
                        {
                            pool.Push(buffer);
                            buffer = null;
                            break;
                        }
                    }
                }
            }
        }

        public int BufferSize
        {
            get
            {
                return this.bufferSize;
            }
        }

        public static bool EnableReleaseTracking
        {
            get
            {
                return enableReleaseTracking;
            }
            set
            {
                enableReleaseTracking = value;
            }
        }

        internal static class NativeMethods
        {
            [DllImport("kernel32.dll")]
            private static extern uint GetCurrentProcessorNumber();

            internal static int CurrentProcessor
            {
                get
                {
                    return (int)GetCurrentProcessorNumber();
                }
            }
        }
    }
}
