﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Timers;


namespace WbxmlDocument
{
    internal class ThrottlingObjectPool<DataType>  where DataType : new()
    {
        private readonly object acquireMonitor;
        private readonly Stack<DataType> bufferPool;
        private readonly object createMonitor;
        private int poolCapacity;
        private readonly ManualResetEvent poolNotEmptyEvent;
        private bool throttlingObjectCreation;
        private System.Timers.Timer timer;

        public ThrottlingObjectPool()
            : this(Environment.ProcessorCount)
        {
        }

        public ThrottlingObjectPool(int initialAllocatedObjects)
            : this(initialAllocatedObjects, 0x7fffffff)
        {
        }

        public ThrottlingObjectPool(int initialAllocatedObjects, int maximumPoolSize)
        {
            this.acquireMonitor = new object();
            this.createMonitor = new object();
            this.poolNotEmptyEvent = new ManualResetEvent(false);
            if (initialAllocatedObjects < 0)
            {
                throw new ArgumentOutOfRangeException("initialAllocatedObjects", initialAllocatedObjects, "Should not be negative.");
            }
            if (maximumPoolSize < 1)
            {
                throw new ArgumentOutOfRangeException("maximumPoolSize", maximumPoolSize, "Needs be be >= 1.");
            }
            if (maximumPoolSize < initialAllocatedObjects)
            {
                throw new ArgumentOutOfRangeException("maximumPoolSize", maximumPoolSize, "Needs to be >= initialAllocatedObjects.");
            }
            this.poolCapacity = maximumPoolSize;
            int num = Math.Max(0x10, initialAllocatedObjects);
            int capacity = Math.Min(maximumPoolSize, num);
            this.bufferPool = new Stack<DataType>(capacity);
            for (int i = 0; i < initialAllocatedObjects; i++)
            {
                this.Release(this.AllocateNewObject());
            }
        }

        public DataType Acquire()
        {
            return this.Acquire(0);
        }

        public DataType Acquire(int waitCycle)
        {
            DataType local = default(DataType);
            bool flag = false;
            try
            {
                while (!flag)
                {
                    flag = Monitor.TryEnter(this.acquireMonitor, waitCycle);
                    if (flag)
                    {
                        if (this.poolNotEmptyEvent.WaitOne(waitCycle))
                        {
                            lock (this.bufferPool)
                            {
                                local = this.bufferPool.Pop();
                                if (this.bufferPool.Count == 0)
                                {
                                    this.poolNotEmptyEvent.Reset();
                                }
                                continue;
                            }
                        }
                        return this.AllocateNewObject();
                    }
                    if (!this.throttlingObjectCreation)
                    {
                        lock (this.createMonitor)
                        {
                            if (!this.throttlingObjectCreation)
                            {
                                try
                                {
                                    this.throttlingObjectCreation = true;
                                    return this.AllocateNewObject();
                                }
                                finally
                                {
                                    if (waitCycle > 0)
                                    {
                                        this.timer = new System.Timers.Timer((double)waitCycle);
                                        this.timer.AutoReset = false;
                                        this.timer.Elapsed += new ElapsedEventHandler(this.ReleaseObjectCreationThrottle);
                                        this.timer.Enabled = true;
                                    }
                                    else
                                    {
                                        this.throttlingObjectCreation = false;
                                    }
                                }
                            }
                            continue;
                        }
                    }
                }
            }
            finally
            {
                if (flag)
                {
                    Monitor.Exit(this.acquireMonitor);
                }
            }
            return local;
        }

        private DataType AllocateNewObject()
        {
            this.TotalBuffersAllocated++;
            if (default(DataType) != null)
            {
                return default(DataType);
            }
            return Activator.CreateInstance<DataType>();
        }

        public void Release(DataType value)
        {
            if (this.bufferPool.Count < this.poolCapacity)
            {
                lock (this.bufferPool)
                {
                    this.bufferPool.Push(value);
                    if (1 == this.bufferPool.Count)
                    {
                        this.poolNotEmptyEvent.Set();
                    }
                    return;
                }
            }
            IDisposable disposable = value as IDisposable;
            if (disposable != null)
            {
                disposable.Dispose();
            }
        }

        private void ReleaseObjectCreationThrottle(object source, ElapsedEventArgs eventArgs)
        {
            lock (this.createMonitor)
            {
                this.throttlingObjectCreation = false;
                if (this.timer != null)
                {
                    this.timer.Dispose();
                    this.timer = null;
                }
            }
        }

        private int totalBuffersAllocated;
        public int TotalBuffersAllocated
        {
            [CompilerGenerated]
            get
            {
                return totalBuffersAllocated;
            }
            [CompilerGenerated]
            private set
            {
                this.totalBuffersAllocated = value;
            }
        }
    }
}
