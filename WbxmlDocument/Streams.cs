﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace WbxmlDocument
{
    internal static class Streams
    {
        public static Stream CloneTemporaryStorageStream(Stream originalStream)
        {
            ICloneableStream stream = originalStream as ICloneableStream;
            if (stream == null)
            {
                throw new ArgumentException("This stream cannot be cloned", "originalStream");
            }
            return stream.Clone();
        }

        public static void ConfigureTempStorage(int defaultMaximumSize, int defaultBlockSize, string defaultPath, Func<int, byte[]> defaultAcquireBuffer, Action<byte[]> defaultReleaseBuffer)
        {
            TemporaryDataStorage.Configure(defaultMaximumSize, defaultBlockSize, defaultPath, defaultAcquireBuffer, defaultReleaseBuffer);
        }

        public static Stream CreateAutoPositionedStream(Stream baseStream)
        {
            return new AutoPositionReadOnlyStream(baseStream, true);
        }

        public static Stream CreateSuppressCloseWrapper(Stream baseStream)
        {
            return new SuppressCloseStream(baseStream);
        }

        public static Stream CreateTemporaryStorageStream()
        {
            return ApplicationServices.Provider.CreateTemporaryStorage();
        }

        public static Stream CreateTemporaryStorageStream(Func<int, byte[]> acquireBuffer, Action<byte[]> releaseBuffer)
        {
            return DefaultApplicationServices.CreateTemporaryStorage(acquireBuffer, releaseBuffer);
        }

        public static string GetCtsTempPath()
        {
            return TemporaryDataStorage.GetTempPath();
        }
    }
}
