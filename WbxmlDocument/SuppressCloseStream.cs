﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace WbxmlDocument
{
    internal sealed class SuppressCloseStream : Stream, ICloneableStream
    {
        // Fields
        private Stream sourceStream;

        // Methods
        public SuppressCloseStream(Stream sourceStream)
        {
            if (sourceStream == null)
            {
                throw new ArgumentNullException("sourceStream");
            }
            this.sourceStream = sourceStream;
        }

        private void AssertOpen()
        {
            if (this.sourceStream == null)
            {
                throw new ObjectDisposedException("SuppressCloseStream");
            }
        }

        public Stream Clone()
        {
            this.AssertOpen();
            if (this.CanWrite)
            {
                throw new NotSupportedException();
            }
            ICloneableStream sourceStream = this.sourceStream as ICloneableStream;
            if (sourceStream == null)
            {
                if (!this.sourceStream.CanSeek)
                {
                    throw new NotSupportedException();
                }
                this.sourceStream = new AutoPositionReadOnlyStream(this.sourceStream, false);
                sourceStream = this.sourceStream as ICloneableStream;
            }
            return new SuppressCloseStream(sourceStream.Clone());
        }

        protected override void Dispose(bool disposing)
        {
            this.sourceStream = null;
            base.Dispose(disposing);
        }

        public override void Flush()
        {
            this.AssertOpen();
            this.sourceStream.Flush();
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            this.AssertOpen();
            return this.sourceStream.Read(buffer, offset, count);
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            this.AssertOpen();
            return this.sourceStream.Seek(offset, origin);
        }

        public override void SetLength(long value)
        {
            this.AssertOpen();
            this.sourceStream.SetLength(value);
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            this.AssertOpen();
            this.sourceStream.Write(buffer, offset, count);
        }

        // Properties
        public override bool CanRead
        {
            get
            {
                return ((this.sourceStream != null) && this.sourceStream.CanRead);
            }
        }

        public override bool CanSeek
        {
            get
            {
                return ((this.sourceStream != null) && this.sourceStream.CanSeek);
            }
        }

        public override bool CanWrite
        {
            get
            {
                return ((this.sourceStream != null) && this.sourceStream.CanWrite);
            }
        }

        public override long Length
        {
            get
            {
                this.AssertOpen();
                return this.sourceStream.Length;
            }
        }

        public override long Position
        {
            get
            {
                this.AssertOpen();
                return this.sourceStream.Position;
            }
            set
            {
                this.AssertOpen();
                this.sourceStream.Position = value;
            }
        }
    }

 

}
