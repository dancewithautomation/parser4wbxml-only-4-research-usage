﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Xml;

namespace WbxmlDocument
{
    public class WbxmlReader : WbxmlBase, IDisposable
    {
        private byte currentTagPage;
        private static XmlDocument errorDocument;
        private readonly bool parseBlobAsByteArray;
        private static BufferPool readerPool = new BufferPool(0x400, true);
        private Stream stream;
        private Stream underlyingStream;

        public WbxmlReader(Stream stream)
            : this(stream, false)
        {
        }

        public WbxmlReader(Stream stream, bool parseBlobAsByteArray)
        {
            this.parseBlobAsByteArray = parseBlobAsByteArray;
            if (stream == null)
            {
                throw new WbxmlException("WbxmlReader cannot accept a null stream");
            }
            this.underlyingStream = stream;
            this.stream = new PooledBufferedStream(stream, readerPool, false);
        }

        private static SecureString ConvertToSecureString(byte[] buffer)
        {
            SecureString str2;
            GCHandle handle = new GCHandle();
            try
            {
                SecureString str = new SecureString();
                char[] chars = Encoding.UTF8.GetChars(buffer);
                handle = GCHandle.Alloc(chars, GCHandleType.Pinned);
                Array.Clear(buffer, 0, buffer.Length);
                for (int i = 0; i < chars.Length; i++)
                {
                    str.AppendChar(chars[i]);
                    chars[i] = '\0';
                }
                str2 = str;
            }
            finally
            {
                if (handle.IsAllocated)
                {
                    handle.Free();
                }
            }
            return str2;
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.underlyingStream = null;
                if (this.stream != null)
                {
                    this.stream.Dispose();
                    this.stream = null;
                }
            }
        }

        private bool FillXmlElement(XmlElement elem, int depth, bool elemIsSecureData, bool elemIsBlobData)
        {
            if (depth > 20)
            {
                throw new WbxmlException("Document nested too deep");
            }
            while (true)
            {
                bool flag;
                bool flag2;
                int tag = this.ReadTag(out flag, out flag2);
                switch (tag)
                {
                    case 1:
                        return true;

                    case 3:
                        if (elemIsSecureData)
                        {
                            AirSyncSecureStringXmlNode node = (AirSyncSecureStringXmlNode)elem;
                            node.SecureData = this.ReadInlineSecureString();
                        }
                        else
                        {
                            string str = this.ReadInlineString();
                            elem.InnerText = str;
                        }
                        break;

                    default:
                        if (tag == 0xc3)
                        {
                            if (elemIsSecureData)
                            {
                                AirSyncSecureStringXmlNode node2 = (AirSyncSecureStringXmlNode)elem;
                                node2.SecureData = this.ReadOpaqueSecureString();
                            }
                            else if (elemIsBlobData)
                            {
                                AirSyncBlobXmlNode node3 = elem as AirSyncBlobXmlNode;
                                int count = this.ReadMultiByteInteger();
                                if ((this.stream.Position + count) > this.stream.Length)
                                {
                                    return false;
                                }
                                if (this.parseBlobAsByteArray)
                                {
                                    node3.ByteArray = this.ReadBytes(count);
                                }
                                else
                                {
                                    node3.Stream = new SubStream(this.underlyingStream, this.stream.Position, (long)count);
                                    this.stream.Seek((long)count, SeekOrigin.Current);
                                }
                            }
                            else
                            {
                                string str2 = this.ReadOpaqueString();
                                elem.InnerText = str2;
                            }
                        }
                        break;
                }
                if ((tag & 0x3f) >= 5)
                {
                    XmlElement element;
                    if (flag)
                    {
                        this.SkipAttributes();
                    }
                    string name = WbxmlBase.Schema.GetName(tag);
                    string nameSpace = WbxmlBase.Schema.GetNameSpace(tag);
                    if ((name == null) || (nameSpace == null))
                    {
                        return false;
                    }
                    bool flag3 = WbxmlBase.Schema.IsTagSecure(tag);
                    bool flag4 = WbxmlBase.Schema.IsTagAnOpaqueBlob(tag);
                    if (flag3)
                    {
                        element = new AirSyncSecureStringXmlNode(null, name, nameSpace, elem.OwnerDocument);
                    }
                    else if (flag4)
                    {
                        element = new AirSyncBlobXmlNode(null, name, nameSpace, elem.OwnerDocument);
                    }
                    else
                    {
                        element = elem.OwnerDocument.CreateElement(name, nameSpace);
                    }
                    if (flag2 && !this.FillXmlElement(element, depth + 1, flag3, flag4))
                    {
                        return false;
                    }
                    elem.AppendChild(element);
                }
            }
        }

        private byte ReadByte()
        {
            int num = this.stream.ReadByte();
            if (num == -1)
            {
                Exception innerException = new EndOfStreamException();
                throw new WbxmlException("End of stream reached by ReadByte before parsing was complete", innerException);
            }
            return (byte)num;
        }

        private byte[] ReadBytes(int count)
        {
            byte[] answer = new byte[count];
            this.ReadBytes(answer);
            return answer;
        }

        private void ReadBytes(byte[] answer)
        {
            int length = answer.Length;
            int offset = 0;
            while (offset < length)
            {
                int num3 = this.stream.Read(answer, offset, length - offset);
                if (num3 == 0)
                {
                    break;
                }
                offset += num3;
            }
            if (offset != length)
            {
                Exception innerException = new EndOfStreamException();
                throw new WbxmlException("End of stream reached by ReadBytes before parsing was complete", innerException);
            }
        }

        private void ReadHeader()
        {
            if (this.ReadByte() != 3)
            {
                throw new WbxmlException("Unsupported Wbxml version");
            }
            int num2 = this.ReadMultiByteInteger();
            if (num2 != 1)
            {
                throw new WbxmlException("Unsupported PublicID: " + num2);
            }
            int num3 = this.ReadMultiByteInteger();
            if (num3 != 0x6a)
            {
                throw new WbxmlException("Unsupported charset: 0x" + num3.ToString("X", CultureInfo.InvariantCulture));
            }
            if (this.ReadMultiByteInteger() > 0)
            {
                throw new WbxmlException("Unsupported charset: 0x" + num3.ToString("X", CultureInfo.InvariantCulture));
            }
        }

        private SecureString ReadInlineSecureString()
        {
            SecureString str;
            long position = this.stream.Position;
            long num2 = position;
            while (this.ReadByte() != 0)
            {
                num2 += 1L;
            }
            this.stream.Position = position;
            GCHandle handle = new GCHandle();
            try
            {
                int num3 = ((int)(num2 - position)) + 1;
                byte[] buffer = new byte[num3];
                handle = GCHandle.Alloc(buffer, GCHandleType.Pinned);
                this.ReadBytes(buffer);
                str = ConvertToSecureString(buffer);
            }
            finally
            {
                if (handle.IsAllocated)
                {
                    handle.Free();
                }
            }
            return str;
        }

        private string ReadInlineString()
        {
            string str;
            long position = this.stream.Position;
            int offset = 0;
            int num3 = 0;
            byte[] buffer2 = null;
            try
            {
                byte[] buffer = buffer2 = readerPool.Acquire();
                while (true)
                {
                    int num4 = this.stream.Read(buffer, offset, buffer.Length - offset);
                    if (num4 == 0)
                    {
                        Exception innerException = new EndOfStreamException();
                        throw new WbxmlException("End of stream reached by ReadInlineString before parsing was complete", innerException);
                    }
                    num3 += num4;
                    while ((offset < num3) && (buffer[offset] != 0))
                    {
                        offset++;
                    }
                    if (offset < num3)
                    {
                        this.stream.Seek((position + offset) + 1L, SeekOrigin.Begin);
                        break;
                    }
                    Array.Resize<byte>(ref buffer, buffer.Length * 2);
                }
                str = Encoding.UTF8.GetString(buffer, 0, offset);
            }
            finally
            {
                if (buffer2 != null)
                {
                    readerPool.Release(buffer2);
                }
            }
            return str;
        }

        private int ReadMultiByteInteger()
        {
            uint num = 0;
            byte num2 = this.ReadByte();
            int num3 = 1;
        Label_000B:
            if (num3 > 5)
            {
                throw new WbxmlException("Multi-byte integer is too large");
            }
            num = (num << 7) + ((uint)(num2 & 0x7f));
            if ((num2 & 0x80) != 0)
            {
                num2 = this.ReadByte();
                num3++;
                goto Label_000B;
            }
            if (num > 0x7fffffff)
            {
                throw new WbxmlException("Multi-byte integer is too large");
            }
            return (int)num;
        }

        private SecureString ReadOpaqueSecureString()
        {
            SecureString str;
            GCHandle handle = new GCHandle();
            try
            {
                byte[] buffer = new byte[this.ReadMultiByteInteger()];
                handle = GCHandle.Alloc(buffer, GCHandleType.Pinned);
                this.ReadBytes(buffer);
                str = ConvertToSecureString(buffer);
            }
            finally
            {
                if (handle.IsAllocated)
                {
                    handle.Free();
                }
            }
            return str;
        }

        private string ReadOpaqueString()
        {
            int count = this.ReadMultiByteInteger();
            byte[] bytes = this.ReadBytes(count);
            return Encoding.UTF8.GetString(bytes);
        }

        private int ReadTag(out bool hasAttributes, out bool hasContent)
        {
            byte num = this.ReadByte();
            int num2 = 0;
            while (num == 0)
            {
                this.currentTagPage = this.ReadByte();
                if (num2++ > 5)
                {
                    throw new WbxmlException("Multiple consecutive page change codes not allowed");
                }
                num = this.ReadByte();
            }
            if ((num & 0x3f) < 5)
            {
                hasAttributes = false;
                hasContent = (num & 0x40) != 0;
                return num;
            }
            hasAttributes = (num & 0x80) != 0;
            hasContent = (num & 0x40) != 0;
            num = (byte)(num & 0x3f);
            return (num | (this.currentTagPage << 8));
        }

        public XmlDocument ReadXmlDocument()
        {
            XmlDocument document2;
            this.ReadHeader();
            try
            {
                bool flag;
                bool flag2;
                XmlElement element;
                int tag = this.ReadTag(out flag, out flag2);
                string name = WbxmlBase.Schema.GetName(tag);
                string nameSpace = WbxmlBase.Schema.GetNameSpace(tag);
                if ((name == null) || (nameSpace == null))
                {
                    return ErrorDocument;
                }
                XmlDocument doc = new SafeXmlDocument();
                bool elemIsSecureData = WbxmlBase.Schema.IsTagSecure(tag);
                bool elemIsBlobData = WbxmlBase.Schema.IsTagAnOpaqueBlob(tag);
                if (elemIsSecureData)
                {
                    element = new AirSyncSecureStringXmlNode(null, name, nameSpace, doc);
                }
                else if (elemIsBlobData)
                {
                    element = new AirSyncBlobXmlNode(null, name, nameSpace, doc);
                }
                else
                {
                    element = doc.CreateElement(name, nameSpace);
                }
                doc.AppendChild(element);
                if (flag)
                {
                    this.SkipAttributes();
                }
                if (flag2 && !this.FillXmlElement(element, 0, elemIsSecureData, elemIsBlobData))
                {
                    return ErrorDocument;
                }
                document2 = doc;
            }
            catch (IndexOutOfRangeException exception)
            {
                throw new WbxmlException("Invalid WBXML code/codepage from client", exception);
            }
            return document2;
        }

        private void SkipAttributes()
        {
            byte num = this.ReadByte();
        Label_0007:
            switch (num)
            {
                case 1:
                    return;

                case 0:
                    {
                        this.ReadByte();
                        break;
                    }
                case 4:
                    {
                        this.ReadMultiByteInteger();
                        break;
                    }
                case 3:
                    {
                        num = this.ReadByte();
                        while (num != 0)
                        {
                            num = this.ReadByte();
                        }
                        break;
                    }
            }
            switch (num)
            {
                case 0x83:
                    this.ReadMultiByteInteger();
                    goto Label_0007;

                case 0xc3:
                    {
                        int count = this.ReadMultiByteInteger();
                        this.ReadBytes(count);
                        goto Label_0007;
                    }
            }
            if ((num & 0x3f) < 5)
            {
                throw new WbxmlException("Unsupported control code");
            }
            throw new WbxmlException("Invalid WBXML - Attributes (potentially malicious)");
        }

        private static XmlDocument ErrorDocument
        {
            get
            {
                if (errorDocument == null)
                {
                    string xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?><WbxmlConversionError></WbxmlConversionError>";
                    XmlDocument document = new SafeXmlDocument();
                    document.LoadXml(xml);
                    errorDocument = document;
                }
                return errorDocument;
            }
        }
    }
}
