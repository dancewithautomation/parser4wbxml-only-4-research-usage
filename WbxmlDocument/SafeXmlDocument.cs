﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

namespace WbxmlDocument
{
    internal class SafeXmlDocument : XmlDocument
    {
        // Methods
        public SafeXmlDocument()
        {
        }

        public SafeXmlDocument(XmlImplementation imp)
        {
            throw new NotSupportedException("Not supported");
        }

        public SafeXmlDocument(XmlNameTable nt)
            : base(nt)
        {
        }

        public override void Load(Stream inStream)
        {
            XmlTextReader reader = new XmlTextReader(inStream)
            {
                EntityHandling = EntityHandling.ExpandCharEntities,
                DtdProcessing = DtdProcessing.Prohibit,
                XmlResolver = null
            };
            this.Load(reader);
        }

        public override void Load(TextReader txtReader)
        {
            using (XmlTextReader reader = new XmlTextReader(txtReader))
            {
                reader.EntityHandling = EntityHandling.ExpandCharEntities;
                reader.DtdProcessing = DtdProcessing.Prohibit;
                reader.XmlResolver = null;
                this.Load(reader);
            }
        }

        public override void Load(string filename)
        {
            using (XmlTextReader reader = new XmlTextReader(filename))
            {
                reader.EntityHandling = EntityHandling.ExpandCharEntities;
                reader.DtdProcessing = DtdProcessing.Prohibit;
                reader.XmlResolver = null;
                this.Load(reader);
            }
        }

        public override void Load(XmlReader reader)
        {
            if ((reader.Settings != null) && (reader.Settings.DtdProcessing != DtdProcessing.Prohibit))
            {
                throw new XmlDtdException();
            }
            try
            {
                base.Load(reader);
            }
            catch (XmlException exception)
            {
                if (exception.Message.StartsWith("DTD is prohibited in this XML document.", StringComparison.OrdinalIgnoreCase))
                {
                    throw new XmlDtdException();
                }
                throw;
            }
        }

        public override void LoadXml(string xml)
        {
            using (XmlTextReader reader = new XmlTextReader(new StringReader(xml)))
            {
                reader.EntityHandling = EntityHandling.ExpandCharEntities;
                reader.DtdProcessing = DtdProcessing.Prohibit;
                reader.XmlResolver = null;
                base.Load(reader);
            }
        }
    }

 

}
