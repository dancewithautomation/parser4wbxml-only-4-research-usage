﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WbxmlDocument
{
    public abstract class WbxmlSchema
    {
        private int version;

        public WbxmlSchema()
        {
            throw new WbxmlException("WbxmlSchema should never be created.  Create one of the derived classes.");
        }

        protected WbxmlSchema(int airSyncVersion)
        {
            this.version = airSyncVersion;
        }

        public abstract string GetName(int tag);
        public abstract string GetNameSpace(int tag);
        public abstract int GetTag(string nameSpace, string name);
        public abstract bool IsTagAnOpaqueBlob(int tag);
        public abstract bool IsTagSecure(int tag);

        public int Version
        {
            get
            {
                return this.version;
            }
        }
    }
}
