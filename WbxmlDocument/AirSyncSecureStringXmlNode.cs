﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Xml;

namespace WbxmlDocument
{
    internal class AirSyncSecureStringXmlNode : XmlElement
    {
        private SecureString secureData;

        public AirSyncSecureStringXmlNode(string prefix, string localName, string namespaceURI, XmlDocument doc)
            : base(prefix, localName, namespaceURI, doc)
        {
        }

        public SecureString DetachSecureData()
        {
            SecureString secureData = this.secureData;
            this.secureData = null;
            return secureData;
        }

        public SecureString SecureData
        {
            set
            {
                this.secureData = value;
                this.InnerText = "******";
            }
        }
    }
}
