﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace WbxmlDocument
{
  internal abstract class DataStorage : RefCountable
{
    // Fields
    protected bool isReadOnly;
    protected object readOnlyLock;

    // Methods
    protected DataStorage()
    {
    }

    public virtual long CopyContentToStream(long start, long end, Stream destStream, ref byte[] scratchBuffer)
    {
        base.ThrowIfDisposed();
        if ((destStream == null) && (end != 0x7fffffffffffffffL))
        {
            return (end - start);
        }
        using (Stream stream = this.OpenReadStream(start, end))
        {
            return CopyStreamToStream(stream, destStream, 0x7fffffffffffffffL, ref scratchBuffer);
        }
    }

    public static long CopyStreamToStream(Stream srcStream, Stream destStream, long lengthToCopy, ref byte[] scratchBuffer)
    {
        if ((scratchBuffer == null) || (scratchBuffer.Length < 0x1000))
        {
            scratchBuffer = new byte[0x1000];
        }
        long num = 0L;
        while (lengthToCopy != 0L)
        {
            int count = (int) Math.Min(lengthToCopy, (long) scratchBuffer.Length);
            int num3 = srcStream.Read(scratchBuffer, 0, count);
            if (num3 == 0)
            {
                return num;
            }
            if (destStream != null)
            {
                destStream.Write(scratchBuffer, 0, num3);
            }
            num += num3;
            if (lengthToCopy != 0x7fffffffffffffffL)
            {
                lengthToCopy -= num3;
            }
        }
        return num;
    }

    public static Stream NewEmptyReadStream()
    {
        return new StreamOnReadableDataStorage(null, 0L, 0L);
    }

    public abstract Stream OpenReadStream(long start, long end);
    internal virtual void SetReadOnly(bool makeReadOnly)
    {
        base.ThrowIfDisposed();
        if (makeReadOnly != this.isReadOnly)
        {
            if (makeReadOnly)
            {
                this.readOnlyLock = new object();
            }
            else
            {
                this.readOnlyLock = null;
            }
            this.isReadOnly = makeReadOnly;
        }
    }

    // Properties
    internal bool IsReadOnly
    {
        get
        {
            return this.isReadOnly;
        }
    }
}

}
