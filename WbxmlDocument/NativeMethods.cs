﻿using Microsoft.Win32.SafeHandles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;

namespace WbxmlDocument
{
    [ComVisible(false), SuppressUnmanagedCodeSecurity]
    internal class NativeMethods
    {
        // Fields
        internal const uint CREATE_ALWAYS = 2;
        internal const uint CREATE_NEW = 1;
        internal const uint DELETE = 0x10000;
        internal const int ERROR_ACCESS_DENIED = 5;
        internal const int ERROR_ALREADY_EXISTS = 0xb7;
        internal const int ERROR_FILE_EXISTS = 80;
        internal const uint FILE_ADD_FILE = 2;
        internal const uint FILE_ADD_SUBDIRECTORY = 4;
        internal const uint FILE_ALL_ACCESS = 0x1f01ff;
        internal const uint FILE_APPEND_DATA = 4;
        internal const uint FILE_ATTRIBUTE_ARCHIVE = 0x20;
        internal const uint FILE_ATTRIBUTE_COMPRESSED = 0x800;
        internal const uint FILE_ATTRIBUTE_DEVICE = 0x40;
        internal const uint FILE_ATTRIBUTE_DIRECTORY = 0x10;
        internal const uint FILE_ATTRIBUTE_ENCRYPTED = 0x4000;
        internal const uint FILE_ATTRIBUTE_HIDDEN = 2;
        internal const uint FILE_ATTRIBUTE_NORMAL = 0x80;
        internal const uint FILE_ATTRIBUTE_NOT_CONTENT_INDEXED = 0x2000;
        internal const uint FILE_ATTRIBUTE_OFFLINE = 0x1000;
        internal const uint FILE_ATTRIBUTE_READONLY = 1;
        internal const uint FILE_ATTRIBUTE_REPARSE_POuint = 0x400;
        internal const uint FILE_ATTRIBUTE_SPARSE_FILE = 0x200;
        internal const uint FILE_ATTRIBUTE_SYSTEM = 4;
        internal const uint FILE_ATTRIBUTE_TEMPORARY = 0x100;
        internal const uint FILE_CREATE_PIPE_INSTANCE = 4;
        internal const uint FILE_DELETE_CHILD = 0x40;
        internal const uint FILE_EXECUTE = 0x20;
        internal const uint FILE_FLAG_BACKUP_SEMANTICS = 0x2000000;
        internal const uint FILE_FLAG_DELETE_ON_CLOSE = 0x4000000;
        internal const uint FILE_FLAG_FIRST_PIPE_INSTANCE = 0x80000;
        internal const uint FILE_FLAG_NO_BUFFERING = 0x20000000;
        internal const uint FILE_FLAG_OPEN_NO_RECALL = 0x100000;
        internal const uint FILE_FLAG_OPEN_REPARSE_POuint = 0x200000;
        internal const uint FILE_FLAG_OVERLAPPED = 0x40000000;
        internal const uint FILE_FLAG_POSIX_SEMANTICS = 0x1000000;
        internal const uint FILE_FLAG_RANDOM_ACCESS = 0x10000000;
        internal const uint FILE_FLAG_SEQUENTIAL_SCAN = 0x8000000;
        internal const uint FILE_FLAG_WRITE_THROUGH = 0x80000000;
        internal const uint FILE_GENERIC_EXECUTE = 0x1200a0;
        internal const uint FILE_GENERIC_READ = 0x120089;
        internal const uint FILE_GENERIC_WRITE = 0x120116;
        internal const uint FILE_LIST_DIRECTORY = 1;
        internal const uint FILE_READ_ATTRIBUTES = 0x80;
        internal const uint FILE_READ_DATA = 1;
        internal const uint FILE_READ_EA = 8;
        internal const uint FILE_SHARE_DELETE = 4;
        internal const uint FILE_SHARE_READ = 1;
        internal const uint FILE_SHARE_WRITE = 2;
        internal const uint FILE_TRAVERSE = 0x20;
        internal const uint FILE_WRITE_ATTRIBUTES = 0x100;
        internal const uint FILE_WRITE_DATA = 2;
        internal const uint FILE_WRITE_EA = 0x10;
        internal const int MAX_PATH = 260;
        internal const uint OPEN_ALWAYS = 4;
        internal const uint OPEN_EXISTING = 3;
        internal const uint READ_CONTROL = 0x20000;
        internal const uint SPECIFIC_RIGHTS_ALL = 0xffff;
        internal const uint STANDARD_RIGHTS_ALL = 0x1f0000;
        internal const uint STANDARD_RIGHTS_EXECUTE = 0x20000;
        internal const uint STANDARD_RIGHTS_READ = 0x20000;
        internal const uint STANDARD_RIGHTS_REQUIRED = 0xf0000;
        internal const uint STANDARD_RIGHTS_WRITE = 0x20000;
        internal const uint SYNCHRONIZE = 0x100000;
        internal const uint TRUNCATE_EXISTING = 5;
        internal const uint WRITE_DAC = 0x40000;
        internal const uint WRITE_OWNER = 0x80000;

        // Methods
        [DllImport("kernel32.dll", EntryPoint = "CreateFileW", CharSet = CharSet.Unicode, SetLastError = true)]
        internal static extern SafeFileHandle CreateFile([In] string filename, [In] uint accessMode, [In] uint shareMode, ref SecurityAttributes securityAttributes, [In] uint creationDisposition, [In] uint flags, [In] IntPtr templateFileHandle);

        // Nested Types
        [StructLayout(LayoutKind.Sequential)]
        internal struct SecurityAttributes
        {
            internal int length;
            internal IntPtr securityDescriptor;
            [MarshalAs(UnmanagedType.Bool)]
            internal bool inheritHandle;
            internal SecurityAttributes(bool inheritHandle)
            {
                this.length = Marshal.SizeOf(typeof(NativeMethods.SecurityAttributes));
                this.securityDescriptor = IntPtr.Zero;
                this.inheritHandle = inheritHandle;
            }
        }
    }

 

}
