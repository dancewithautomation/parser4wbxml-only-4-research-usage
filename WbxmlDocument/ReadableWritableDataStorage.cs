﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WbxmlDocument
{
   internal abstract class ReadableWritableDataStorage : ReadableDataStorage
{
    // Methods
    public virtual StreamOnDataStorage OpenWriteStream(bool append)
    {
        base.ThrowIfDisposed();
        if (append)
        {
            return new AppendStreamOnDataStorage(this);
        }
        return new ReadWriteStreamOnDataStorage(this);
    }

    public abstract void SetLength(long length);
    public abstract void Write(long position, byte[] buffer, int offset, int count);
}

}
