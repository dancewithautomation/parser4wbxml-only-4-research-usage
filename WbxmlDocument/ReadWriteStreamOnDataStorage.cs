﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace WbxmlDocument
{
  internal class ReadWriteStreamOnDataStorage : StreamOnDataStorage, ICloneableStream
{
    // Fields
    private long position;
    private ReadableWritableDataStorage storage;

    // Methods
    internal ReadWriteStreamOnDataStorage(ReadableWritableDataStorage storage)
    {
        storage.AddRef();
        this.storage = storage;
    }

    private ReadWriteStreamOnDataStorage(ReadableWritableDataStorage storage, long position)
    {
        storage.AddRef();
        this.storage = storage;
        this.position = position;
    }

    protected override void Dispose(bool disposing)
    {
        if (disposing && (this.storage != null))
        {
            this.storage.Release();
            this.storage = null;
        }
        base.Dispose(disposing);
    }

    public override void Flush()
    {
        if (this.storage == null)
        {
            throw new ObjectDisposedException("ReadWriteStreamOnDataStorage");
        }
    }

    Stream ICloneableStream.Clone()
    {
        if (this.storage == null)
        {
            throw new ObjectDisposedException("ReadWriteStreamOnDataStorage");
        }
        return new ReadWriteStreamOnDataStorage(this.storage, this.position);
    }

    public override int Read(byte[] buffer, int offset, int count)
    {
        if (this.storage == null)
        {
            throw new ObjectDisposedException("ReadWriteStreamOnDataStorage");
        }
        if (buffer == null)
        {
            throw new ArgumentNullException("buffer");
        }
        if ((offset > buffer.Length) || (offset < 0))
        {
            throw new ArgumentOutOfRangeException("offset", "OffsetOutOfRange");
        }
        if (count < 0)
        {
            throw new ArgumentOutOfRangeException("count", "CountOutOfRange");
        }
        if ((count + offset) > buffer.Length)
        {
            throw new ArgumentOutOfRangeException("count", "CountTooLarge");
        }
        int num = this.storage.Read(this.position, buffer, offset, count);
        this.position += num;
        return num;
    }

    public override long Seek(long offset, SeekOrigin origin)
    {
        if (this.storage == null)
        {
            throw new ObjectDisposedException("ReadWriteStreamOnDataStorage");
        }
        switch (origin)
        {
            case SeekOrigin.Begin:
                this.position = offset;
                break;

            case SeekOrigin.Current:
                offset = this.position + offset;
                break;

            case SeekOrigin.End:
                offset = this.storage.Length + offset;
                break;

            default:
                throw new ArgumentException("Invalid Origin enumeration value", "origin");
        }
        if (offset < 0L)
        {
            throw new ArgumentOutOfRangeException("offset", "CannotSeekBeforeBeginning");
        }
        this.position = offset;
        return this.position;
    }

    public override void SetLength(long value)
    {
        if (this.storage == null)
        {
            throw new ObjectDisposedException("ReadWriteStreamOnDataStorage");
        }
        if (value < 0L)
        {
            throw new ArgumentOutOfRangeException("value", "CannotSetNegativelength");
        }
        this.storage.SetLength(value);
    }

    public override void Write(byte[] buffer, int offset, int count)
    {
        if (this.storage == null)
        {
            throw new ObjectDisposedException("ReadWriteStreamOnDataStorage");
        }
        if (buffer == null)
        {
            throw new ArgumentNullException("buffer");
        }
        if ((offset > buffer.Length) || (offset < 0))
        {
            throw new ArgumentOutOfRangeException("offset", "OffsetOutOfRange");
        }
        if ((count > buffer.Length) || (count < 0))
        {
            throw new ArgumentOutOfRangeException("count", "CountOutOfRange");
        }
        if ((count + offset) > buffer.Length)
        {
            throw new ArgumentOutOfRangeException("count", "CountTooLarge");
        }
        this.storage.Write(this.position, buffer, offset, count);
        this.position += count;
    }

    // Properties
    public override bool CanRead
    {
        get
        {
            if (this.storage == null)
            {
                return false;
            }
            return true;
        }
    }

    public override bool CanSeek
    {
        get
        {
            if (this.storage == null)
            {
                return false;
            }
            return true;
        }
    }

    public override bool CanWrite
    {
        get
        {
            if (this.storage == null)
            {
                return false;
            }
            return true;
        }
    }

    public override long End
    {
        get
        {
            return 0x7fffffffffffffffL;
        }
    }

    public override long Length
    {
        get
        {
            if (this.storage == null)
            {
                throw new ObjectDisposedException("ReadWriteStreamOnDataStorage");
            }
            return this.storage.Length;
        }
    }

    public override long Position
    {
        get
        {
            if (this.storage == null)
            {
                throw new ObjectDisposedException("ReadWriteStreamOnDataStorage");
            }
            return this.position;
        }
        set
        {
            if (this.storage == null)
            {
                throw new ObjectDisposedException("ReadWriteStreamOnDataStorage");
            }
            if (value < 0L)
            {
                throw new ArgumentOutOfRangeException("value", "CannotSeekBeforeBeginning");
            }
            this.position = value;
        }
    }

    public override long Start
    {
        get
        {
            return 0L;
        }
    }

    public override DataStorage Storage
    {
        get
        {
            if (this.storage == null)
            {
                throw new ObjectDisposedException("ReadWriteStreamOnDataStorage");
            }
            return this.storage;
        }
    }
}

}
