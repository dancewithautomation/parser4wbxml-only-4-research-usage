﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace WbxmlDocument
{
    internal abstract class RefCountable : IDisposable
{
    // Fields
    private bool isDisposed;
    private int refCount = 1;

    // Methods
    protected RefCountable()
    {
    }

    public void AddRef()
    {
        this.ThrowIfDisposed();
        Interlocked.Increment(ref this.refCount);
    }

    public void Dispose()
    {
        this.Dispose(true);
        GC.SuppressFinalize(this);
    }

    protected virtual void Dispose(bool disposing)
    {
        this.isDisposed = true;
    }

    public void Release()
    {
        this.ThrowIfDisposed();
        if (Interlocked.Decrement(ref this.refCount) == 0)
        {
            this.Dispose();
        }
    }

    protected void ThrowIfDisposed()
    {
        if (this.isDisposed)
        {
            throw new ObjectDisposedException("RefCountable");
        }
    }

    // Properties
    protected bool IsDisposed
    {
        get
        {
            return this.isDisposed;
        }
    }

    public int RefCount
    {
        get
        {
            return this.refCount;
        }
    }
}


}
