﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace WbxmlDocument
{
    internal class XmlDtdException : XmlException
    {
        // Properties
        public override string Message
        {
            get
            {
                return "For security reasons DTD is prohibited in this XML document.";
            }
        }
    }

 

}
