﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace WbxmlDocument
{
    internal class SubStream : Stream,IDisposable
    {
        private long length;
        private long startPosition;
        private Stream stream;

        public SubStream(Stream stream, long startPosition, long length)
        {
            this.stream = stream;
            this.startPosition = startPosition;
            this.length = length;
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        public override void Flush()
        {
            this.stream.Flush();
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            if ((this.stream.Position + count) > (this.startPosition + this.length))
            {
                count = (int)((this.startPosition + this.length) - this.stream.Position);
            }
            if (count > 0)
            {
                return this.stream.Read(buffer, offset, count);
            }
            return 0;
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            long startPosition = 0L;
            if (origin == SeekOrigin.Begin)
            {
                startPosition = this.startPosition + offset;
            }
            else if (origin == SeekOrigin.Current)
            {
                startPosition = this.stream.Position + offset;
            }
            else
            {
                if (origin != SeekOrigin.End)
                {
                    throw new ArgumentException();
                }
                startPosition = this.length + offset;
            }
            if (startPosition < this.startPosition)
            {
                startPosition = this.startPosition;
            }
            if (startPosition > (this.startPosition + this.length))
            {
                startPosition = this.startPosition + this.length;
            }
            return this.stream.Seek(startPosition, SeekOrigin.Begin);
        }

        public override void SetLength(long value)
        {
            throw new NotImplementedException("The SubStream class doesn't allow changing the length");
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            throw new NotImplementedException("The SubStream class doesn't support writing");
        }

        public override bool CanRead
        {
            get
            {
                return this.stream.CanRead;
            }
        }

        public override bool CanSeek
        {
            get
            {
                return this.stream.CanSeek;
            }
        }

        public override bool CanWrite
        {
            get
            {
                return false;
            }
        }

        public override long Length
        {
            get
            {
                return this.length;
            }
        }

        public override long Position
        {
            get
            {
                return (this.stream.Position - this.startPosition);
            }
            set
            {
                if (value > this.length)
                {
                    this.stream.Position = this.startPosition + this.length;
                }
                else
                {
                    this.stream.Position = this.startPosition + value;
                }
            }
        }
    }
}
