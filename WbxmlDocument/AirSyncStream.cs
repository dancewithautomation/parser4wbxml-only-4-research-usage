﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace WbxmlDocument
{
    internal class AirSyncStream : Stream, IDisposable
    {
        private static int defaultMaxBufferCountPerProcessor = (10 * 0x10);
        private Stream internalStream;
        private static volatile BufferPool streamBufferPool;
        private static object streamBufferPoolLock = new object();
        private int streamHash;

        public AirSyncStream()
        {
            this.internalStream = Streams.CreateTemporaryStorageStream(new Func<int, byte[]>(AirSyncStream.AcquireBuffer), new Action<byte[]>(AirSyncStream.ReleaseBuffer));
        }

        private static byte[] AcquireBuffer(int bufferSize)
        {
            if (streamBufferPool == null)
            {
                lock (streamBufferPoolLock)
                {
                    if (streamBufferPool == null)
                    {
                        streamBufferPool = new BufferPool(bufferSize, defaultMaxBufferCountPerProcessor);
                    }
                }
            }
            if (bufferSize != streamBufferPool.BufferSize)
            {
                throw new InvalidOperationException(string.Format(CultureInfo.InvariantCulture, "bufferSize '{0}' is different from streamBufferPool.BufferSize '{1}'", new object[] { bufferSize, streamBufferPool.BufferSize }));
            }
            return streamBufferPool.Acquire();
        }

        public void CopyStream(Stream outputStream, int count)
        {
            if (outputStream == null)
            {
                throw new ArgumentNullException("outputStream");
            }
            if (this.DoBase64Conversion)
            {
                StreamHelper.CopyStreamWithBase64Conversion(this.internalStream, outputStream, count, true);
            }
            else
            {
                StreamHelper.CopyStream(this.internalStream, outputStream, count);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.internalStream != null)
                {
                    this.internalStream.Dispose();
                    this.internalStream = null;
                }
            }
            base.Dispose(disposing);
        }

        public override void Flush()
        {
            this.internalStream.Flush();
        }

        private int GetStreamHash()
        {
            byte[] buffer;
            int num = 0;
            uint num2 = 0;
            long position = this.internalStream.Position;
            this.internalStream.Position = 0L;
            bool flag = false;
            if (streamBufferPool != null)
            {
                buffer = streamBufferPool.Acquire();
                flag = true;
            }
            else
            {
                buffer = new byte[0x2000];
            }
            try
            {
                int num4;
            Label_0046:
                num4 = this.internalStream.Read(buffer, 0, buffer.Length);
                if (num4 != 0)
                {
                    num2 = StreamHelper.UpdateCrc32(num2, buffer, 0, num4);
                    num += num4;
                    if (num < this.Length)
                    {
                        goto Label_0046;
                    }
                }
                this.internalStream.Position = position;
            }
            finally
            {
                if (flag && (buffer != null))
                {
                    streamBufferPool.Release(buffer);
                }
            }
            return (int)num2;
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            if (buffer == null)
            {
                throw new ArgumentNullException("buffer");
            }
            if ((buffer.Length - offset) < count)
            {
                throw new ArgumentException(string.Format(CultureInfo.InstalledUICulture, "buffer is not long enough: buffer.Length={0}, offset={1}, count={2}", new object[] { buffer.Length, offset, count }));
            }
            if (this.DoBase64Conversion)
            {
                int byteCount = (((count * 3) / 4) < 1) ? 1 : ((count * 3) / 4);
                using (MemoryStream stream = new MemoryStream(buffer))
                {
                    return StreamHelper.CopyStreamWithBase64Conversion(this.internalStream, stream, byteCount, true);
                }
            }
            return this.internalStream.Read(buffer, offset, count);
        }

        private static void ReleaseBuffer(byte[] buffer)
        {
            if (buffer == null)
            {
                throw new ArgumentNullException("buffer");
            }
            streamBufferPool.Release(buffer);
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            return this.internalStream.Seek(offset, origin);
        }

        public override void SetLength(long value)
        {
            this.internalStream.SetLength(value);
        }

        public override string ToString()
        {
            long position = this.Position;
            this.Position = 0L;
            string str = new StreamReader(this, Encoding.UTF8).ReadToEnd();
            this.Position = position;
            return str;
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            this.streamHash = 0;
            this.internalStream.Write(buffer, offset, count);
        }

        public override bool CanRead
        {
            get
            {
                return this.internalStream.CanRead;
            }
        }

        public override bool CanSeek
        {
            get
            {
                return this.internalStream.CanSeek;
            }
        }

        public override bool CanWrite
        {
            get
            {
                return this.internalStream.CanWrite;
            }
        }

        public bool DoBase64Conversion { get; set; }

        public override long Length
        {
            get
            {
                long length = this.internalStream.Length;
                if (this.DoBase64Conversion)
                {
                    long num2 = (length / 3L) * 4L;
                    return (num2 + (((length % 3L) != 0L) ? ((long)4) : ((long)0)));
                }
                return length;
            }
        }

        public override long Position
        {
            get
            {
                return this.internalStream.Position;
            }
            set
            {
                this.internalStream.Position = value;
            }
        }

        public int StreamHash
        {
            get
            {
                if ((this.streamHash == 0) && (this.internalStream.Length > 0L))
                {
                    this.streamHash = this.GetStreamHash();
                }
                return this.streamHash;
            }
            set
            {
                this.streamHash = value;
            }
        }
    }
}
