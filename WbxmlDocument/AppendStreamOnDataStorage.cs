﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace WbxmlDocument
{
    internal class AppendStreamOnDataStorage : StreamOnDataStorage
{
    // Fields
    private long position;
    private ReadableWritableDataStorage storage;

    // Methods
    public AppendStreamOnDataStorage(ReadableWritableDataStorage storage)
    {
        storage.AddRef();
        this.storage = storage;
        this.position = storage.Length;
    }

    protected override void Dispose(bool disposing)
    {
        if (disposing && (this.storage != null))
        {
            this.storage.Release();
            this.storage = null;
        }
        base.Dispose(disposing);
    }

    public override void Flush()
    {
        if (this.storage == null)
        {
            throw new ObjectDisposedException("AppendStreamOnDataStorage");
        }
    }

    public override int Read(byte[] buffer, int offset, int count)
    {
        throw new NotSupportedException();
    }

    public override long Seek(long offset, SeekOrigin origin)
    {
        throw new NotSupportedException();
    }

    public override void SetLength(long value)
    {
        throw new NotSupportedException();
    }

    public override void Write(byte[] buffer, int offset, int count)
    {
        if (this.storage == null)
        {
            throw new ObjectDisposedException("AppendStreamOnDataStorage");
        }
        if (buffer == null)
        {
            throw new ArgumentNullException("buffer");
        }
        if ((offset > buffer.Length) || (offset < 0))
        {
            throw new ArgumentOutOfRangeException("offset", "OffsetOutOfRange");
        }
        if ((count > buffer.Length) || (count < 0))
        {
            throw new ArgumentOutOfRangeException("count", "CountOutOfRange");
        }
        if ((count + offset) > buffer.Length)
        {
            throw new ArgumentOutOfRangeException("count", "CountTooLarge");
        }
        this.storage.Write(this.position, buffer, offset, count);
        this.position += count;
    }

    // Properties
    public override bool CanRead
    {
        get
        {
            return false;
        }
    }

    public override bool CanSeek
    {
        get
        {
            return false;
        }
    }

    public override bool CanWrite
    {
        get
        {
            return (this.storage != null);
        }
    }

    public override long End
    {
        get
        {
            return this.position;
        }
    }

    public override long Length
    {
        get
        {
            throw new NotSupportedException();
        }
    }

    public override long Position
    {
        get
        {
            throw new NotSupportedException();
        }
        set
        {
            throw new NotSupportedException();
        }
    }

    public ReadableWritableDataStorage ReadableWritableStorage
    {
        get
        {
            return this.storage;
        }
    }

    public override long Start
    {
        get
        {
            return 0L;
        }
    }

    public override DataStorage Storage
    {
        get
        {
            return this.storage;
        }
    }
}

}
