﻿using Microsoft.Win32.SafeHandles;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Text;
using System.Threading;

namespace WbxmlDocument
{
    internal class TempFileStream : FileStream
    {
        // Fields
        private string filePath;
        private static int nextId = (Environment.TickCount ^ Process.GetCurrentProcess().Id);
        private static string tempPath;

        // Methods
        private TempFileStream(SafeFileHandle handle)
            : base(handle, FileAccess.ReadWrite)
        {
        }

        public static TempFileStream CreateInstance()
        {
            return CreateInstance("Cts");
        }

        public static TempFileStream CreateInstance(string prefix)
        {
            return CreateInstance(prefix, true);
        }

        public static TempFileStream CreateInstance(string prefix, bool deleteOnClose)
        {
            uint num3;
            NativeMethods.SecurityAttributes securityAttributes = new NativeMethods.SecurityAttributes(false);
            string path = Path;
            new FileIOPermission(FileIOPermissionAccess.Write, path).Demand();
            int error = 0;
            int num2 = 10;
        Label_0026:
            num3 = (uint)Interlocked.Increment(ref nextId);
            string filename = System.IO.Path.Combine(path, prefix + num3.ToString("X5") + ".tmp");
            uint num4 = deleteOnClose ? (uint)0x4000000 : 0;
            SafeFileHandle handle = NativeMethods.CreateFile(filename, 0x12019f, 0, ref securityAttributes, 1, (0x100 | num4) | 0x2000, IntPtr.Zero);
            num2--;
            if (handle.IsInvalid)
            {
                error = Marshal.GetLastWin32Error();
                if (error == 80)
                {
                    num2++;
                }
                using (Process process = Process.GetCurrentProcess())
                {
                    Interlocked.Add(ref nextId, process.Id);
                    goto Label_00CB;
                }
            }
            num2 = 0;
        Label_00CB:
            if (num2 > 0)
            {
                goto Label_0026;
            }
            if (handle.IsInvalid)
            {
                string message = "CreateFileFailed(filename)";
                throw new IOException(message, new Win32Exception(error, message));
            }
            return new TempFileStream(handle) { filePath = filename };
        }

        protected override void Dispose(bool disposing)
        {
            try
            {
                base.Dispose(disposing);
            }
            catch (IOException)
            {
            }
        }

        private static string GetTempPath()
        {
            if (tempPath == null)
            {
                tempPath = System.IO.Path.GetTempPath();
            }
            return tempPath;
        }

        internal static void SetTemporaryPath(string path)
        {
            tempPath = path;
        }

        // Properties
        public string FilePath
        {
            get
            {
                return this.filePath;
            }
        }

        internal static string Path
        {
            get
            {
                return GetTempPath();
            }
        }
    }

 

}
