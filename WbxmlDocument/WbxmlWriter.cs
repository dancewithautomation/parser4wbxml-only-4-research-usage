﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

namespace WbxmlDocument
{
    public class WbxmlWriter : WbxmlBase
    {
        private byte currentTagPage;
        private Stream stream;
        private const byte WbxmlVersion = 3;

        public WbxmlWriter(Stream stream)
        {
            if (stream == null)
            {
                throw new WbxmlException("WbxmlWriter cannot take a null stream parameter");
            }
            this.stream = stream;
        }

        private static bool ElementHasContent(XmlElement elem)
        {
            foreach (XmlNode node in elem.ChildNodes)
            {
                if ((((node.NodeType == XmlNodeType.Element) || ((node.NodeType == XmlNodeType.Text) && !string.IsNullOrEmpty(node.Value))) || ((node.NodeType == XmlNodeType.CDATA) && !string.IsNullOrEmpty(node.Value))) || (node.NodeType == XmlNodeType.Document))
                {
                    return true;
                }
            }
            return false;
        }

        private void WriteByteArray(byte[] data)
        {
            this.stream.WriteByte(0xc3);
            this.WriteMultiByteInteger(data.Length);
            this.stream.Write(data, 0, data.Length);
        }

        private void WriteEndTag()
        {
            this.stream.WriteByte(1);
        }

        private void WriteHeader()
        {
            this.currentTagPage = 0;
            this.WriteMultiByteInteger(3);
            this.WriteMultiByteInteger(1);
            this.WriteMultiByteInteger(0x6a);
            this.WriteMultiByteInteger(0);
        }

        private void WriteMultiByteInteger(int value)
        {
            if (value > 0x7f)
            {
                this.WriteMultiByteIntegerRecursive(value >> 7);
            }
            this.stream.WriteByte((byte)(value & 0x7f));
        }

        private void WriteMultiByteIntegerRecursive(int value)
        {
            if (value > 0x7f)
            {
                this.WriteMultiByteIntegerRecursive(value >> 7);
            }
            this.stream.WriteByte((byte)((value & 0x7f) | 0x80));
        }

        private void WriteOpaqueString(string str)
        {
            this.stream.WriteByte(0xc3);
            byte[] bytes = Encoding.UTF8.GetBytes(str);
            this.WriteMultiByteInteger(bytes.Length);
            this.stream.Write(bytes, 0, bytes.Length);
        }

        private void WriteString(string str)
        {
            this.stream.WriteByte(3);
            byte[] bytes = Encoding.UTF8.GetBytes(str);
            this.stream.Write(bytes, 0, bytes.Length);
            this.stream.WriteByte(0);
        }

        private void WriteTag(int tag, bool hasContent, bool hasAttributes)
        {
            byte num = (byte)((tag & 0xff00) >> 8);
            byte num2 = (byte)(tag & 0x3f);
            if (hasContent)
            {
                num2 = (byte)(num2 + 0x40);
            }
            if (hasAttributes)
            {
                num2 = (byte)(num2 + 0x80);
            }
            if (num != this.currentTagPage)
            {
                this.stream.WriteByte(0);
                this.stream.WriteByte(num);
                this.currentTagPage = num;
            }
            this.stream.WriteByte(num2);
        }

        public void WriteXmlDocument(XmlDocument doc)
        {
            if (doc == null)
            {
                throw new WbxmlException("WbxmlWriter cannot take a null XmlDocument parameter");
            }
            this.WriteHeader();
            this.WriteXmlElement(doc.DocumentElement);
        }

        public void WriteXmlDocumentFromElement(XmlElement elem)
        {
            if (elem == null)
            {
                throw new WbxmlException("WbxmlWriter cannot take a null XmlElement parameter");
            }
            this.WriteHeader();
            this.WriteXmlElement(elem);
        }

        private void WriteXmlElement(XmlElement elem)
        {
            int tag = WbxmlBase.Schema.GetTag(elem.NamespaceURI, elem.LocalName);
            bool hasContent = ElementHasContent(elem);
            this.WriteTag(tag, hasContent, false);
            if (hasContent)
            {
                foreach (XmlNode node in elem.ChildNodes)
                {
                    switch (node.NodeType)
                    {
                        case XmlNodeType.Element:
                            break;

                        case XmlNodeType.Attribute:
                            {
                                continue;
                            }
                        case XmlNodeType.Text:
                            {
                                this.WriteString(node.Value);
                                continue;
                            }
                        case XmlNodeType.CDATA:
                            {
                                this.WriteOpaqueString(node.Value);
                                continue;
                            }
                        default:
                            throw new ArgumentOutOfRangeException("AirSyncPermanentException");
                    }
                    AirSyncBlobXmlNode node2 = node as AirSyncBlobXmlNode;
                    if (node2 != null)
                    {
                        int num2 = WbxmlBase.Schema.GetTag(node2.NamespaceURI, node2.LocalName);
                        this.WriteTag(num2, true, false);
                        if (node2.ByteArray != null)
                        {
                            this.WriteByteArray(node2.ByteArray);
                        }
                        if (node2.Stream != null)
                        {
                            switch (node2.OriginalNodeType)
                            {
                                case XmlNodeType.Text:
                                    this.stream.WriteByte(3);
                                    break;

                                case XmlNodeType.CDATA:
                                    this.stream.WriteByte(0xc3);
                                    this.WriteMultiByteInteger((int)node2.StreamDataSize);
                                    break;
                            }
                            node2.CopyStream(this.stream);
                            if (node2.OriginalNodeType == XmlNodeType.Text)
                            {
                                this.stream.WriteByte(0);
                            }
                        }
                        this.WriteEndTag();
                        continue;
                    }
                    this.WriteXmlElement((XmlElement)node);
                }
                this.WriteEndTag();
            }
        }
    }
}
