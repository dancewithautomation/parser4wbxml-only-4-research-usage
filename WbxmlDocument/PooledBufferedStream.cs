﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace WbxmlDocument
{
    internal sealed class PooledBufferedStream : Stream, IDisposable
    {
        private byte[] buffer;
        private long bufferOffset;
        private readonly int bufferSize;
        private int bufferTopBorder;
        private readonly bool closeStream;
        private Stream internalStream;
        private bool isClosed;
        private bool isWriteMode;
        private BufferPool pool;
        private long position;

        public PooledBufferedStream(Stream stream, BufferPool bufferPool)
        {
            this.closeStream = true;
            if (stream == null)
            {
                throw new ArgumentNullException("stream");
            }
            if (bufferPool == null)
            {
                throw new ArgumentNullException("bufferPool");
            }
            if (!stream.CanRead)
            {
                if (!stream.CanWrite)
                {
                    throw new ArgumentException("ImmutableStream", "stream");
                }
                this.isWriteMode = true;
            }
            this.pool = bufferPool;
            this.bufferSize = bufferPool.BufferSize;
            this.internalStream = stream;
            if (this.internalStream.CanSeek)
            {
                this.position = this.internalStream.Position;
            }
            this.bufferOffset = this.position;
        }

        public PooledBufferedStream(Stream stream, BufferPoolCollection.BufferSize bufferSize)
            : this(stream, BufferPoolCollection.AutoCleanupCollection.Acquire(bufferSize))
        {
        }

        public PooledBufferedStream(Stream stream, BufferPool bufferPool, bool closeStream)
            : this(stream, bufferPool)
        {
            this.closeStream = closeStream;
        }

        [Conditional("DEBUG")]
        private void CheckDisposed(string methodName)
        {
            if (this.isClosed)
            {
                throw new ObjectDisposedException(base.GetType().ToString(), methodName);
            }
        }

        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    this.isClosed = true;
                    if (this.internalStream != null)
                    {
                        try
                        {
                            if (this.isWriteMode)
                            {
                                this.FlushBuffer();
                            }
                        }
                        finally
                        {
                            if (this.closeStream)
                            {
                                this.internalStream.Dispose();
                                this.internalStream = null;
                            }
                        }
                    }
                }
            }
            finally
            {
                base.Dispose(disposing);
                if ((disposing && (this.buffer != null)) && (this.pool != null))
                {
                    this.pool.Release(this.buffer);
                    this.buffer = null;
                    this.pool = null;
                }
            }
        }

        private void EnsureBufferAcquired()
        {
            if (this.buffer == null)
            {
                this.buffer = this.pool.Acquire();
            }
        }

        private void EnsureReading()
        {
            if (this.isWriteMode)
            {
                if (!this.internalStream.CanRead)
                {
                    throw new NotSupportedException();
                }
                if (this.bufferTopBorder != 0)
                {
                    this.UpdateInternalStreamPositionForWriting();
                    this.internalStream.Write(this.buffer, 0, this.bufferTopBorder);
                }
                this.isWriteMode = false;
            }
        }

        private void EnsureWriting()
        {
            if (!this.isWriteMode)
            {
                if (!this.internalStream.CanWrite || !this.internalStream.CanSeek)
                {
                    throw new NotSupportedException();
                }
                this.bufferTopBorder = 0;
                this.bufferOffset = this.position;
                this.isWriteMode = true;
            }
        }

        private bool FillBuffer()
        {
            this.bufferOffset = this.position;
            this.EnsureBufferAcquired();
            this.bufferTopBorder = this.internalStream.Read(this.buffer, 0, this.bufferSize);
            return (this.bufferTopBorder != 0);
        }

        public override void Flush()
        {
            if (!this.internalStream.CanWrite)
            {
                throw new NotSupportedException();
            }
            if (this.isWriteMode)
            {
                this.FlushBuffer();
            }
            this.internalStream.Flush();
        }

        public Stream FlushAndTakeWrappedStream()
        {
            this.Flush();
            Stream internalStream = this.internalStream;
            this.internalStream = null;
            return internalStream;
        }

        private void FlushBuffer()
        {
            if (this.bufferTopBorder != 0)
            {
                this.UpdateInternalStreamPositionForWriting();
                this.internalStream.Write(this.buffer, 0, this.bufferTopBorder);
                this.bufferTopBorder = 0;
            }
            this.bufferOffset = this.position;
        }

        private void FlushIfBufferFull()
        {
            if (this.bufferTopBorder == this.bufferSize)
            {
                this.FlushBuffer();
            }
        }

        private int FlushIfPositionOutOfBuffer()
        {
            if ((this.position < this.bufferOffset) || (this.position > (this.bufferOffset + this.bufferTopBorder)))
            {
                this.FlushBuffer();
            }
            return (int)(this.position - this.bufferOffset);
        }

        public override int Read(byte[] destination, int offset, int count)
        {
            this.EnsureReading();
            if (!this.HasDataBufferedForReading)
            {
                this.UpdateInternalStreamPositionForReading();
                if (count >= this.bufferSize)
                {
                    int num = this.internalStream.Read(destination, offset, count);
                    this.position += num;
                    return num;
                }
                if (!this.FillBuffer())
                {
                    return 0;
                }
            }
            int sourceIndex = (int)(this.position - this.bufferOffset);
            int length = Math.Min(count, this.bufferTopBorder - sourceIndex);
            Array.Copy(this.buffer, sourceIndex, destination, offset, length);
            this.position += length;
            return length;
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            if (!this.internalStream.CanSeek)
            {
                throw new NotSupportedException();
            }
            switch (origin)
            {
                case SeekOrigin.Begin:
                    this.position = offset;
                    break;

                case SeekOrigin.Current:
                    this.position += offset;
                    break;

                case SeekOrigin.End:
                    if (this.isWriteMode)
                    {
                        this.FlushBuffer();
                    }
                    this.internalStream.Seek(offset, SeekOrigin.End);
                    this.position = this.internalStream.Position;
                    break;
            }
            return this.position;
        }

        public override void SetLength(long value)
        {
            if (!this.internalStream.CanSeek)
            {
                throw new NotSupportedException();
            }
            this.EnsureWriting();
            this.FlushBuffer();
            if (this.internalStream.Position != this.position)
            {
                this.internalStream.Position = this.position;
            }
            this.internalStream.SetLength(value);
            this.position = this.internalStream.Position;
        }

        private void UpdateInternalStreamPositionForReading()
        {
            if (this.internalStream.CanSeek && (this.internalStream.Position != this.position))
            {
                this.internalStream.Position = this.position;
            }
        }

        private void UpdateInternalStreamPositionForWriting()
        {
            if (this.internalStream.CanSeek && (this.internalStream.Position != this.bufferOffset))
            {
                this.internalStream.Position = this.bufferOffset;
            }
        }

        public override void Write(byte[] source, int offset, int count)
        {
            this.EnsureWriting();
            int destinationIndex = this.FlushIfPositionOutOfBuffer();
            if ((destinationIndex == 0) && (count > this.bufferSize))
            {
                this.UpdateInternalStreamPositionForWriting();
                this.internalStream.Write(source, offset, count);
                this.position += count;
                this.bufferOffset = this.position;
            }
            else
            {
                int length = Math.Min(this.bufferSize - destinationIndex, count);
                this.EnsureBufferAcquired();
                Array.Copy(source, offset, this.buffer, destinationIndex, length);
                this.bufferTopBorder = Math.Max(destinationIndex + length, this.bufferTopBorder);
                this.position += length;
                this.FlushIfBufferFull();
                if (length < count)
                {
                    this.Write(source, offset + length, count - length);
                }
            }
        }

        public override bool CanRead
        {
            get
            {
                return this.internalStream.CanRead;
            }
        }

        public override bool CanSeek
        {
            get
            {
                return this.internalStream.CanSeek;
            }
        }

        public override bool CanWrite
        {
            get
            {
                return this.internalStream.CanWrite;
            }
        }

        private bool HasDataBufferedForReading
        {
            get
            {
                return ((this.position < (this.bufferOffset + this.bufferTopBorder)) && (this.position >= this.bufferOffset));
            }
        }

        public override long Length
        {
            get
            {
                long length = this.internalStream.Length;
                long num2 = (this.bufferTopBorder != 0) ? (this.bufferOffset + this.bufferTopBorder) : 0L;
                return Math.Max(length, num2);
            }
        }

        public override long Position
        {
            get
            {
                if (!this.internalStream.CanSeek)
                {
                    throw new NotSupportedException();
                }
                return this.position;
            }
            set
            {
                if (!this.internalStream.CanSeek)
                {
                    throw new NotSupportedException();
                }
                this.position = value;
            }
        }
    }
}
