﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace WbxmlDocument
{
    internal class WbxmlSchema30 : WbxmlSchema
    {
        private CodePage30[] codeSpace30;
        private Hashtable stringSpace30;

        public WbxmlSchema30()
            : base(30)
        {
            this.codeSpace30 = new CodePage30[] { 
                new CodePage30(AirSyncStringArrays.AirSync, "AirSync:", 0), new CodePage30(AirSyncStringArrays.Contacts, "Contacts:", 1), new CodePage30(AirSyncStringArrays.Email, "Email:", 2), new CodePage30(AirSyncStringArrays.AirNotify, "AirNotify:", 3), new CodePage30(AirSyncStringArrays.Cal, "Calendar:", 4), new CodePage30(AirSyncStringArrays.Move, "Move:", 5), new CodePage30(AirSyncStringArrays.ItemEstimate, "GetItemEstimate:", 6), new CodePage30(AirSyncStringArrays.FolderHierarchy, "FolderHierarchy:", 7), new CodePage30(AirSyncStringArrays.MeetingResponse, "MeetingResponse:", 8), new CodePage30(AirSyncStringArrays.Tasks, "Tasks:", 9), new CodePage30(AirSyncStringArrays.ResolveRecipients, "ResolveRecipients:", 10), new CodePage30(AirSyncStringArrays.ValidateCert, "ValidateCert:", 11), new CodePage30(AirSyncStringArrays.Contacts2, "Contacts2:", 12), new CodePage30(AirSyncStringArrays.Ping, "Ping:", 13), new CodePage30(AirSyncStringArrays.Provision, "Provision:", 14), new CodePage30(AirSyncStringArrays.Search, "Search:", 15), 
                new CodePage30(AirSyncStringArrays.Gal, "Gal:", 0x10), new CodePage30(AirSyncStringArrays.AirsyncBase, "AirSyncBase:", 0x11), new CodePage30(AirSyncStringArrays.Settings, "Settings:", 0x12), new CodePage30(AirSyncStringArrays.DocumentLibrary, "DocumentLibrary:", 0x13), new CodePage30(AirSyncStringArrays.ItemOperations, "ItemOperations:", 20), new CodePage30(AirSyncStringArrays.ComposeMail, "ComposeMail:", 0x15), new CodePage30(AirSyncStringArrays.Email2, "Email2:", 0x16), new CodePage30(AirSyncStringArrays.Notes, "Notes:", 0x17), new CodePage30(AirSyncStringArrays.RightsManagement, "RightsManagement:", 0x18)
             };
            this.stringSpace30 = new Hashtable();
            for (int i = 0; i < this.codeSpace30.Length; i++)
            {
                this.stringSpace30.Add(this.codeSpace30[i].NameSpace, this.codeSpace30[i]);
            }
        }

        public override string GetName(int tag)
        {
            int index = (tag & 0xff00) >> 8;
            string str = this.codeSpace30[index].NameFromCode(tag);
            if ((str == null) || (index != this.codeSpace30[index].PageNumber))
            {
                throw new WbxmlException("Invalid tag code: 0x" + tag.ToString("X", CultureInfo.InvariantCulture));
            }
            return str;
        }

        public override string GetNameSpace(int tag)
        {
            int index = (tag & 0xff00) >> 8;
            string nameSpace = this.codeSpace30[index].NameSpace;
            if (nameSpace == null)
            {
                throw new WbxmlException("Invalid tag pagecode: 0x" + tag.ToString("X", CultureInfo.InvariantCulture));
            }
            return nameSpace;
        }

        public override int GetTag(string nameSpace, string name)
        {
            if ((nameSpace == null) || (name == null))
            {
                throw new WbxmlException("nameSpace and name must both be non-null");
            }
            CodePage30 page = (CodePage30)this.stringSpace30[nameSpace];
            if (page == null)
            {
                throw new WbxmlException("The namespace " + nameSpace + " is invalid in this schema");
            }
            int num = page.CodeFromName(name);
            if (num == 0)
            {
                throw new WbxmlException("The name " + nameSpace + " " + name + " could not be found in schema");
            }
            return num;
        }

        public override bool IsTagAnOpaqueBlob(int tag)
        {
            if (((tag != 0x1510) && (tag != 0x1418)) && (tag != 0xf20))
            {
                return (tag == 0x1609);
            }
            return true;
        }

        public override bool IsTagSecure(int tag)
        {
            if (tag != 0xf1f)
            {
                return (tag == 0x1415);
            }
            return true;
        }

        protected class CodePage30
        {
            private string[] codeTable;
            private string nameSpace;
            private int pageNumber;
            private Hashtable stringTable;

            public CodePage30(string[] tags, string nameSpace, int page)
            {
                this.nameSpace = nameSpace;
                this.pageNumber = page;
                this.stringTable = new Hashtable();
                this.codeTable = new string[tags.Length];
                int num = 5;
                foreach (string str in tags)
                {
                    int num2 = page << 8;
                    this.stringTable.Add(str, num | num2);
                    this.codeTable[num - 5] = str;
                    num++;
                }
            }

            public int CodeFromName(string name)
            {
                object obj2 = this.stringTable[name];
                if (obj2 == null)
                {
                    return 0;
                }
                return (int)obj2;
            }

            public string NameFromCode(int code)
            {
                int index = (code & 0xff) - 5;
                return this.codeTable[index];
            }

            public string NameSpace
            {
                get
                {
                    return this.nameSpace;
                }
            }

            public int PageNumber
            {
                get
                {
                    return this.pageNumber;
                }
            }
        }
    }
}
