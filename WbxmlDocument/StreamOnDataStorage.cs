﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace WbxmlDocument
{
    internal abstract class StreamOnDataStorage : Stream
{
    // Methods
    protected StreamOnDataStorage()
    {
    }

    protected override void Dispose(bool disposing)
    {
        base.Dispose(disposing);
    }

    // Properties
    public abstract long End { get; }

    public abstract long Start { get; }

    public abstract DataStorage Storage { get; }
}


}
