﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace WbxmlDocument
{
   internal abstract class ReadableDataStorage : DataStorage
{
    // Methods
    public override long CopyContentToStream(long start, long end, Stream destStream, ref byte[] scratchBuffer)
    {
        base.ThrowIfDisposed();
        if ((scratchBuffer == null) || (scratchBuffer.Length < 0x1000))
        {
            scratchBuffer = new byte[0x1000];
        }
        long num = 0L;
        long num2 = (end == 0x7fffffffffffffffL) ? 0x7fffffffffffffffL : (end - start);
        while (num2 != 0L)
        {
            int count = (int) Math.Min(num2, (long) scratchBuffer.Length);
            int num4 = this.Read(start, scratchBuffer, 0, count);
            if (num4 == 0)
            {
                return num;
            }
            start += num4;
            destStream.Write(scratchBuffer, 0, num4);
            num += num4;
            if (num2 != 0x7fffffffffffffffL)
            {
                num2 -= num4;
            }
        }
        return num;
    }

    public override Stream OpenReadStream(long start, long end)
    {
        base.ThrowIfDisposed();
        return new StreamOnReadableDataStorage(this, start, end);
    }

    public abstract int Read(long position, byte[] buffer, int offset, int count);

    // Properties
    public abstract long Length { get; }
}

  

}
