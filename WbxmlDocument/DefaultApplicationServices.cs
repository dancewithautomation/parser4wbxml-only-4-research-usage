﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;

namespace WbxmlDocument
{
    internal class DefaultApplicationServices : IApplicationServices
    {
        // Fields
        private volatile Dictionary<string, IList<CtsConfigurationSetting>> configurationSubSections;
        private IList<CtsConfigurationSetting> emptySubSection = new List<CtsConfigurationSetting>();
        private object lockObject = new object();

        // Methods
        public Stream CreateTemporaryStorage()
        {
            return CreateTemporaryStorage(null, null);
        }

        public static Stream CreateTemporaryStorage(Func<int, byte[]> acquireBuffer, Action<byte[]> releaseBuffer)
        {
            TemporaryDataStorage storage = new TemporaryDataStorage(acquireBuffer, releaseBuffer);
            Stream stream = storage.OpenWriteStream(false);
            storage.Release();
            return stream;
        }

        public IList<CtsConfigurationSetting> GetConfiguration(string subSectionName)
        {
            IList<CtsConfigurationSetting> list;
            if (!this.GetConfigurationSubSections().TryGetValue(subSectionName ?? string.Empty, out list))
            {
                return this.emptySubSection;
            }
            return list;
        }

        private Dictionary<string, IList<CtsConfigurationSetting>> GetConfigurationSubSections()
        {
            Dictionary<string, IList<CtsConfigurationSetting>> configurationSubSections = this.configurationSubSections;
            if (configurationSubSections == null)
            {
                CtsConfigurationSection section = null;
                try
                {
                    section = ConfigurationManager.GetSection("CTS") as CtsConfigurationSection;
                }
                catch (ConfigurationErrorsException)
                {
                    this.LogConfigurationErrorEvent();
                }
                CtsConfigurationSetting item = null;
                try
                {
                    string str = ConfigurationManager.AppSettings["TemporaryStoragePath"];
                    if (!string.IsNullOrEmpty(str))
                    {
                        item = new CtsConfigurationSetting("TemporaryStorage");
                        item.AddArgument("Path", str);
                    }
                }
                catch (ConfigurationErrorsException)
                {
                    this.LogConfigurationErrorEvent();
                }
                lock (this.lockObject)
                {
                    configurationSubSections = this.configurationSubSections;
                    if (configurationSubSections != null)
                    {
                        return configurationSubSections;
                    }
                    if (section != null)
                    {
                        configurationSubSections = section.SubSectionsDictionary;
                    }
                    else
                    {
                        configurationSubSections = new Dictionary<string, IList<CtsConfigurationSetting>>();
                        configurationSubSections.Add(string.Empty, new List<CtsConfigurationSetting>());
                    }
                    if (item != null)
                    {
                        IList<CtsConfigurationSetting> list = configurationSubSections[string.Empty];
                        bool flag = false;
                        foreach (CtsConfigurationSetting setting2 in list)
                        {
                            if (string.Equals(setting2.Name, item.Name))
                            {
                                flag = true;
                                break;
                            }
                        }
                        if (!flag)
                        {
                            list.Add(item);
                        }
                    }
                    this.configurationSubSections = configurationSubSections;
                }
            }
            return configurationSubSections;
        }

        public void LogConfigurationErrorEvent()
        {
        }

        public void RefreshConfiguration()
        {
            ConfigurationManager.RefreshSection("appSettings");
            ConfigurationManager.RefreshSection("CTS");
            lock (this.lockObject)
            {
                this.configurationSubSections = null;
            }
        }
    }

 

}
