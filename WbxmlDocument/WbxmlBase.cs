﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WbxmlDocument
{
    public class WbxmlBase
    {
        protected const byte AttributeBit = 0x80;
        protected const int AttributeBitMask = 0xff;
        protected const byte ContentBit = 0x40;
        protected const byte ContinuationBit = 0x80;
        protected const byte MinimumTagValue = 5;
        protected const int NamespaceBitMask = 0xff00;
        protected const byte NumberBitMask = 0x7f;
        protected const byte ValidTagBitMask = 0x3f;
        private static WbxmlSchema wbxmlSchema = new WbxmlSchema30();

        public static WbxmlSchema Schema
        {
            get
            {
                return wbxmlSchema;
            }
        }
    }
}
