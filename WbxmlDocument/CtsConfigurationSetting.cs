﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WbxmlDocument
{
    internal class CtsConfigurationSetting
    {
        // Fields
        private IList<CtsConfigurationArgument> arguments;
        private string name;

        // Methods
        internal CtsConfigurationSetting(string name)
        {
            this.name = name;
            this.arguments = new List<CtsConfigurationArgument>();
        }

        internal void AddArgument(string name, string value)
        {
            this.arguments.Add(new CtsConfigurationArgument(name, value));
        }

        // Properties
        public IList<CtsConfigurationArgument> Arguments
        {
            get
            {
                return this.arguments;
            }
        }

        public string Name
        {
            get
            {
                return this.name;
            }
        }
    }

 

}
