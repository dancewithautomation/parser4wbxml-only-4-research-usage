﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.AccessControl;
using System.Security.Permissions;
using System.Security.Principal;
using System.Text;

namespace WbxmlDocument
{
   internal class TemporaryDataStorage : ReadableWritableDataStorage
{
    // Fields
    private VirtualBuffer buffer;
    private static object configurationLockObject = new object();
    internal static volatile bool Configured = false;
    public static Func<int, byte[]> DefaultAcquireBuffer = null;
    public static int DefaultBufferBlockSize = 0x2000;
    public static int DefaultBufferMaximumSize = (DefaultBufferBlockSize * 0x10);
    public static string DefaultPath = null;
    public static Action<byte[]> DefaultReleaseBuffer = null;
    private static readonly FileSystemAccessRule[] DirectoryAccessRules = new FileSystemAccessRule[] { new FileSystemAccessRule(new SecurityIdentifier(WellKnownSidType.BuiltinAdministratorsSid, null), FileSystemRights.FullControl, InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit, PropagationFlags.None, AccessControlType.Allow), new FileSystemAccessRule(new SecurityIdentifier(WellKnownSidType.LocalSystemSid, null), FileSystemRights.FullControl, InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit, PropagationFlags.None, AccessControlType.Allow), new FileSystemAccessRule(new SecurityIdentifier(WellKnownSidType.NetworkServiceSid, null), FileSystemRights.Synchronize | FileSystemRights.Modify | FileSystemRights.DeleteSubdirectoriesAndFiles, InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit, PropagationFlags.None, AccessControlType.Allow), new FileSystemAccessRule(new SecurityIdentifier(WellKnownSidType.LocalServiceSid, null), FileSystemRights.Synchronize | FileSystemRights.Modify | FileSystemRights.DeleteSubdirectoriesAndFiles, InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit, PropagationFlags.None, AccessControlType.Allow) };
    private long filePosition;
    private Stream fileStream;
    private long totalLength;

    // Methods
    public TemporaryDataStorage() : this(DefaultAcquireBuffer, DefaultReleaseBuffer)
    {
    }

    public TemporaryDataStorage(Func<int, byte[]> acquireBuffer, Action<byte[]> releaseBuffer)
    {
        Configure();
        this.buffer = new VirtualBuffer(DefaultBufferBlockSize, DefaultBufferMaximumSize, (acquireBuffer != null) ? acquireBuffer : DefaultAcquireBuffer, (releaseBuffer != null) ? releaseBuffer : DefaultReleaseBuffer);
    }

    private static void Configure()
    {
        if (!Configured)
        {
            lock (configurationLockObject)
            {
                if (!Configured)
                {
                    int defaultBufferMaximumSize = DefaultBufferMaximumSize;
                    int defaultBufferBlockSize = DefaultBufferBlockSize;
                    string defaultPath = DefaultPath;
                    foreach (CtsConfigurationSetting setting in ApplicationServices.Provider.GetConfiguration(null))
                    {
                        if (setting.Name.Equals("TemporaryStorage", StringComparison.OrdinalIgnoreCase))
                        {
                            foreach (CtsConfigurationArgument argument in setting.Arguments)
                            {
                                if (argument.Name.Equals("Path", StringComparison.OrdinalIgnoreCase))
                                {
                                    defaultPath = argument.Value.Trim();
                                }
                                else if (argument.Name.Equals("MaximumBufferSize", StringComparison.OrdinalIgnoreCase))
                                {
                                    if (!int.TryParse(argument.Value.Trim(), out defaultBufferMaximumSize))
                                    {
                                        ApplicationServices.Provider.LogConfigurationErrorEvent();
                                        defaultBufferMaximumSize = DefaultBufferMaximumSize;
                                    }
                                    else if ((defaultBufferMaximumSize < 0x10) || (defaultBufferMaximumSize > 0x2800))
                                    {
                                        ApplicationServices.Provider.LogConfigurationErrorEvent();
                                        defaultBufferMaximumSize = DefaultBufferMaximumSize;
                                    }
                                    else
                                    {
                                        defaultBufferMaximumSize *= 0x400;
                                    }
                                }
                                else if (argument.Name.Equals("BufferIncrement", StringComparison.OrdinalIgnoreCase))
                                {
                                    if (!int.TryParse(argument.Value.Trim(), out defaultBufferBlockSize))
                                    {
                                        ApplicationServices.Provider.LogConfigurationErrorEvent();
                                        defaultBufferBlockSize = DefaultBufferBlockSize;
                                    }
                                    else if ((defaultBufferBlockSize < 4) || (defaultBufferBlockSize > 0x40))
                                    {
                                        ApplicationServices.Provider.LogConfigurationErrorEvent();
                                        defaultBufferBlockSize = DefaultBufferBlockSize;
                                    }
                                    else
                                    {
                                        defaultBufferBlockSize *= 0x400;
                                    }
                                }
                                else
                                {
                                    ApplicationServices.Provider.LogConfigurationErrorEvent();
                                }
                            }
                        }
                    }
                    if ((defaultBufferMaximumSize < defaultBufferBlockSize) || ((defaultBufferMaximumSize % defaultBufferBlockSize) != 0))
                    {
                        ApplicationServices.Provider.LogConfigurationErrorEvent();
                        defaultBufferMaximumSize = DefaultBufferMaximumSize;
                        defaultBufferBlockSize = DefaultBufferBlockSize;
                    }
                    DefaultBufferMaximumSize = defaultBufferMaximumSize;
                    DefaultBufferBlockSize = defaultBufferBlockSize;
                    string systemTempPath = GetSystemTempPath();
                    if (defaultPath != null)
                    {
                        defaultPath = ValidatePath(defaultPath);
                    }
                    if (defaultPath == null)
                    {
                        defaultPath = systemTempPath;
                    }
                    TempFileStream.SetTemporaryPath(defaultPath);
                    Configured = true;
                }
            }
        }
    }

    public static void Configure(int defaultMaximumSize, int defaultBlockSize, string defaultPath, Func<int, byte[]> defaultAcquireBuffer, Action<byte[]> defaultReleaseBuffer)
    {
        DefaultBufferMaximumSize = defaultMaximumSize;
        DefaultBufferBlockSize = defaultBlockSize;
        DefaultPath = defaultPath;
        DefaultAcquireBuffer = defaultAcquireBuffer;
        DefaultReleaseBuffer = defaultReleaseBuffer;
        Configured = false;
        Configure();
    }

    protected override void Dispose(bool disposing)
    {
        if (!base.IsDisposed)
        {
            if (disposing)
            {
                if (this.fileStream != null)
                {
                    this.fileStream.Dispose();
                }
                this.buffer.Dispose();
            }
            this.fileStream = null;
        }
        base.Dispose(disposing);
    }

    private static DirectorySecurity GetDirectorySecurity()
    {
        DirectorySecurity security = new DirectorySecurity();
        security.SetAccessRuleProtection(true, false);
        using (WindowsIdentity identity = WindowsIdentity.GetCurrent())
        {
            security.SetOwner(identity.User);
            for (int i = 0; i < DirectoryAccessRules.Length; i++)
            {
                security.AddAccessRule(DirectoryAccessRules[i]);
            }
            if ((!identity.User.IsWellKnown(WellKnownSidType.LocalSystemSid) && !identity.User.IsWellKnown(WellKnownSidType.NetworkServiceSid)) && !identity.User.IsWellKnown(WellKnownSidType.LocalServiceSid))
            {
                security.AddAccessRule(new FileSystemAccessRule(identity.User, FileSystemRights.FullControl, InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit, PropagationFlags.None, AccessControlType.Allow));
            }
            return security;
        }
    }

    private static string GetSystemTempPath()
    {
        return Path.GetTempPath();
    }

    internal static string GetTempPath()
    {
        Configure();
        return TempFileStream.Path;
    }

    private int InternalRead(long position, byte[] buffer, int offset, int count)
    {
        int num = 0;
        if (position < this.buffer.MaxBytes)
        {
            num = this.buffer.Read(position, buffer, offset, count);
            offset += num;
            count -= num;
            position += num;
        }
        if (((count == 0) || (position < this.buffer.MaxBytes)) || (this.fileStream == null))
        {
            return num;
        }
        if (this.filePosition != (position - this.buffer.MaxBytes))
        {
            this.fileStream.Position = position - this.buffer.MaxBytes;
        }
        int num2 = this.fileStream.Read(buffer, offset, count);
        this.filePosition = (position - this.buffer.MaxBytes) + num2;
        return (num + num2);
    }

    public override int Read(long position, byte[] buffer, int offset, int count)
    {
        base.ThrowIfDisposed();
        if (base.isReadOnly)
        {
            lock (base.readOnlyLock)
            {
                return this.InternalRead(position, buffer, offset, count);
            }
        }
        return this.InternalRead(position, buffer, offset, count);
    }

    internal static void RefreshConfiguration()
    {
        Configured = false;
    }

    public override void SetLength(long length)
    {
        base.ThrowIfDisposed();
        if (base.isReadOnly)
        {
            throw new InvalidOperationException("Write to read-only DataStorage");
        }
        this.totalLength = length;
        if (length <= this.buffer.MaxBytes)
        {
            this.buffer.SetLength(length);
            if (this.fileStream != null)
            {
                this.fileStream.SetLength(0L);
            }
        }
        else
        {
            this.buffer.SetLength((long) this.buffer.MaxBytes);
            if (this.fileStream == null)
            {
                this.fileStream = TempFileStream.CreateInstance();
                this.filePosition = 0L;
            }
            this.fileStream.SetLength(length - this.buffer.MaxBytes);
        }
    }

    private static string ValidatePath(string path)
    {
        try
        {
            if (Path.IsPathRooted(path))
            {
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path, GetDirectorySecurity());
                }
                new FileIOPermission(FileIOPermissionAccess.Write, path).Demand();
                return path;
            }
            path = null;
        }
        catch (PathTooLongException)
        {
            path = null;
        }
        catch (DirectoryNotFoundException)
        {
            path = null;
        }
        catch (IOException)
        {
            path = null;
        }
        catch (UnauthorizedAccessException)
        {
            path = null;
        }
        catch (ArgumentException)
        {
            path = null;
        }
        catch (NotSupportedException)
        {
            path = null;
        }
        return path;
    }

    public override void Write(long position, byte[] buffer, int offset, int count)
    {
        base.ThrowIfDisposed();
        if (base.isReadOnly)
        {
            throw new InvalidOperationException("Write to read-only DataStorage");
        }
        if (position < this.buffer.MaxBytes)
        {
            int num = this.buffer.Write(position, buffer, offset, count);
            offset += num;
            count -= num;
            position += num;
            if (position > this.totalLength)
            {
                this.totalLength = position;
            }
        }
        if (count != 0)
        {
            if (this.fileStream == null)
            {
                this.fileStream = TempFileStream.CreateInstance();
                this.filePosition = 0L;
            }
            if (this.filePosition != (position - this.buffer.MaxBytes))
            {
                this.fileStream.Position = position - this.buffer.MaxBytes;
            }
            this.fileStream.Write(buffer, offset, count);
            position += count;
            this.filePosition = position - this.buffer.MaxBytes;
            if (position > this.totalLength)
            {
                this.totalLength = position;
            }
        }
    }

    // Properties
    public override long Length
    {
        get
        {
            base.ThrowIfDisposed();
            return this.totalLength;
        }
    }

    // Nested Types
    [StructLayout(LayoutKind.Sequential)]
    private struct VirtualBuffer : IDisposable
    {
        private int maximumSize;
        private int blockSize;
        private long length;
        private byte[] firstBlock;
        private byte[][] followingBlocks;
        private Func<int, byte[]> acquireBuffer;
        private Action<byte[]> releaseBuffer;
        public VirtualBuffer(int blockSize, int maximumSize) : this(blockSize, maximumSize, null, null)
        {
        }

        public VirtualBuffer(int blockSize, int maximumSize, Func<int, byte[]> acquireBuffer, Action<byte[]> releaseBuffer)
        {
            if (((acquireBuffer != null) && (releaseBuffer == null)) || ((acquireBuffer == null) && (releaseBuffer != null)))
            {
                throw new ArgumentException("acquireBuffer and releaseBuffer should be both null or non-null");
            }
            this.blockSize = blockSize;
            this.maximumSize = maximumSize;
            this.length = 0L;
            this.firstBlock = null;
            this.followingBlocks = null;
            this.acquireBuffer = acquireBuffer;
            this.releaseBuffer = releaseBuffer;
        }

        public int MaxBytes
        {
            get
            {
                return this.maximumSize;
            }
        }
        public int BlockSize
        {
            get
            {
                return this.blockSize;
            }
        }
        public int Read(long position, byte[] buffer, int offset, int count)
        {
            if (position >= this.length)
            {
                return 0;
            }
            int num = 0;
            if (position < this.BlockSize)
            {
                int num2 = (int) Math.Min((long) (this.BlockSize - position), (long) (this.length - position));
                if (num2 > count)
                {
                    num2 = count;
                }
                Buffer.BlockCopy(this.firstBlock, (int) position, buffer, offset, num2);
                offset += num2;
                count -= num2;
                position += num2;
                num += num2;
            }
            while (count != 0)
            {
                if (position >= this.length)
                {
                    return num;
                }
                int index = (int) ((position - this.BlockSize) / ((long) this.BlockSize));
                int srcOffset = (int) ((position - this.BlockSize) % ((long) this.BlockSize));
                int num5 = (int) Math.Min((long) (this.BlockSize - srcOffset), this.length - position);
                if (num5 > count)
                {
                    num5 = count;
                }
                Buffer.BlockCopy(this.followingBlocks[index], srcOffset, buffer, offset, num5);
                offset += num5;
                count -= num5;
                position += num5;
                num += num5;
            }
            return num;
        }

        public int Write(long position, byte[] buffer, int offset, int count)
        {
            if (position > this.length)
            {
                this.SetLength(position);
            }
            if (position >= this.MaxBytes)
            {
                return 0;
            }
            int num = 0;
            if (position < this.BlockSize)
            {
                if (this.firstBlock == null)
                {
                    this.firstBlock = this.GetBlock();
                }
                int num2 = (int) Math.Min(this.BlockSize - position, (long) count);
                Buffer.BlockCopy(buffer, offset, this.firstBlock, (int) position, num2);
                offset += num2;
                count -= num2;
                position += num2;
                num += num2;
            }
            while (count != 0)
            {
                if (position >= this.MaxBytes)
                {
                    break;
                }
                if (this.followingBlocks == null)
                {
                    this.followingBlocks = new byte[(this.MaxBytes - this.BlockSize) / this.BlockSize][];
                }
                int index = (int) ((position - this.BlockSize) / ((long) this.BlockSize));
                int dstOffset = (int) ((position - this.BlockSize) % ((long) this.BlockSize));
                if (this.followingBlocks[index] == null)
                {
                    this.followingBlocks[index] = this.GetBlock();
                }
                int num5 = Math.Min(this.BlockSize - dstOffset, count);
                Buffer.BlockCopy(buffer, offset, this.followingBlocks[index], dstOffset, num5);
                offset += num5;
                count -= num5;
                position += num5;
                num += num5;
            }
            if (position > this.length)
            {
                this.length = position;
            }
            return num;
        }

        public void SetLength(long length)
        {
            if (this.length < length)
            {
                if (this.length < this.BlockSize)
                {
                    int num = (int) Math.Min((long) (this.BlockSize - this.length), (long) (length - this.length));
                    if (this.firstBlock == null)
                    {
                        this.firstBlock = this.GetBlock();
                    }
                    else
                    {
                        Array.Clear(this.firstBlock, (int) this.length, num);
                    }
                    this.length += num;
                }
                while (this.length < length)
                {
                    if (this.length >= this.MaxBytes)
                    {
                        return;
                    }
                    if (this.followingBlocks == null)
                    {
                        this.followingBlocks = new byte[(this.MaxBytes - this.BlockSize) / this.BlockSize][];
                    }
                    int index = (int) ((this.length - this.BlockSize) / ((long) this.BlockSize));
                    int num3 = (int) ((this.length - this.BlockSize) % ((long) this.BlockSize));
                    int num4 = (int) Math.Min((long) (this.BlockSize - num3), length - this.length);
                    if (this.followingBlocks[index] == null)
                    {
                        this.followingBlocks[index] = this.GetBlock();
                    }
                    else
                    {
                        Array.Clear(this.followingBlocks[index], num3, num4);
                    }
                    this.length += num4;
                }
            }
            else
            {
                this.length = length;
            }
        }

        public void Dispose()
        {
            if (this.releaseBuffer != null)
            {
                if (this.firstBlock != null)
                {
                    this.releaseBuffer(this.firstBlock);
                    this.firstBlock = null;
                }
                if (this.followingBlocks != null)
                {
                    foreach (byte[] buffer in this.followingBlocks)
                    {
                        if (buffer != null)
                        {
                            this.releaseBuffer(buffer);
                        }
                    }
                    this.followingBlocks = null;
                }
                this.releaseBuffer = null;
            }
            GC.SuppressFinalize(this);
        }

        private byte[] GetBlock()
        {
            if (this.acquireBuffer == null)
            {
                return new byte[this.BlockSize];
            }
            return this.acquireBuffer(this.BlockSize);
        }
    }
}
}
