﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace WbxmlDocument
{
    internal interface IApplicationServices
    {
        // Methods
        Stream CreateTemporaryStorage();
        IList<CtsConfigurationSetting> GetConfiguration(string subSectionName);
        void LogConfigurationErrorEvent();
        void RefreshConfiguration();
    }

 

}
