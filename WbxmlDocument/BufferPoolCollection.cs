﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WbxmlDocument
{
    public class BufferPoolCollection
    {
        private readonly bool cleanBuffers;
        private static BufferPoolCollection collection = new BufferPoolCollection();
        private BufferPool[] pools;

        public BufferPoolCollection()
            : this(true)
        {
        }

        public BufferPoolCollection(bool cleanBuffers)
        {
            this.pools = new BufferPool[] { 
                new BufferPool(0x400, cleanBuffers), new BufferPool(0x800, cleanBuffers), new BufferPool(0x1000, cleanBuffers), new BufferPool(0x2000, cleanBuffers), new BufferPool(0x2800, cleanBuffers), new BufferPool(0x4000, cleanBuffers), new BufferPool(0x5000, cleanBuffers), new BufferPool(0x6000, cleanBuffers), new BufferPool(0x7800, cleanBuffers), new BufferPool(0x8000, cleanBuffers), new BufferPool(0xa000, cleanBuffers), new BufferPool(0xc000, cleanBuffers), new BufferPool(0xc800, cleanBuffers), new BufferPool(0xf000, cleanBuffers), new BufferPool(0x10000, cleanBuffers), new BufferPool(0x11800, cleanBuffers), 
                new BufferPool(0x14000, cleanBuffers), new BufferPool(0x16800, cleanBuffers), new BufferPool(0x18000, cleanBuffers), new BufferPool(0x19000, cleanBuffers), new BufferPool(0x1b800, cleanBuffers), new BufferPool(0x1e000, cleanBuffers), new BufferPool(0x20000, cleanBuffers), new BufferPool(0x40000, cleanBuffers), new BufferPool(0x80000, cleanBuffers), new BufferPool(0x100000, cleanBuffers)
             };
            this.cleanBuffers = cleanBuffers;
        }

        public BufferPool Acquire(BufferSize bufferSize)
        {
            int index = (int)bufferSize;
            if ((index < 0) || (index >= this.pools.Length))
            {
                throw new ArgumentOutOfRangeException("bufferSize");
            }
            return this.pools[index];
        }

        public bool TryMatchBufferSize(int size, out BufferSize result)
        {
            for (int i = 0; i < this.pools.Length; i++)
            {
                if (this.pools[i].BufferSize >= size)
                {
                    result = (BufferSize)i;
                    return true;
                }
            }
            result = BufferSize.Size1M;
            return false;
        }

        public static BufferPoolCollection AutoCleanupCollection
        {
            get
            {
                return collection;
            }
        }

        public bool CleanBuffersOnRelease
        {
            get
            {
                return this.cleanBuffers;
            }
        }

        public enum BufferSize
        {
            Size1K,
            Size2K,
            Size4K,
            Size8K,
            Size10K,
            Size16K,
            Size20K,
            Size24K,
            Size30K,
            Size32K,
            Size40K,
            Size48K,
            Size50K,
            Size60K,
            Size64K,
            Size70K,
            Size80K,
            Size90K,
            Size96K,
            Size100K,
            Size110K,
            Size120K,
            Size128K,
            Size256K,
            Size512K,
            Size1M
        }
    }
}
