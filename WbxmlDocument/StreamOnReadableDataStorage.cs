﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace WbxmlDocument
{
    internal class StreamOnReadableDataStorage : StreamOnDataStorage, ICloneableStream
{
    // Fields
    private ReadableDataStorage baseStorage;
    private bool disposed;
    private long end;
    private long position;
    private long start;

    // Methods
    public StreamOnReadableDataStorage(ReadableDataStorage baseStorage, long start, long end)
    {
        if (baseStorage != null)
        {
            baseStorage.AddRef();
            this.baseStorage = baseStorage;
        }
        this.start = start;
        this.end = end;
    }

    private StreamOnReadableDataStorage(ReadableDataStorage baseStorage, long start, long end, long position)
    {
        if (baseStorage != null)
        {
            baseStorage.AddRef();
            this.baseStorage = baseStorage;
        }
        this.start = start;
        this.end = end;
        this.position = position;
    }

    public Stream Clone()
    {
        this.ThrowIfDisposed();
        return new StreamOnReadableDataStorage(this.baseStorage, this.start, this.end, this.position);
    }

    protected override void Dispose(bool disposing)
    {
        if (disposing && (this.baseStorage != null))
        {
            this.baseStorage.Release();
            this.baseStorage = null;
        }
        this.disposed = true;
        base.Dispose(disposing);
    }

    public override void Flush()
    {
        throw new NotSupportedException();
    }

    public override int Read(byte[] buffer, int offset, int count)
    {
        this.ThrowIfDisposed();
        if (buffer == null)
        {
            throw new ArgumentNullException("buffer");
        }
        if ((offset > buffer.Length) || (offset < 0))
        {
            throw new ArgumentOutOfRangeException("offset", "OffsetOutOfRange");
        }
        if (count < 0)
        {
            throw new ArgumentOutOfRangeException("count", "CountOutOfRange");
        }
        if ((count + offset) > buffer.Length)
        {
            throw new ArgumentOutOfRangeException("count", "CountTooLarge");
        }
        int num = 0;
        if ((this.end == 0x7fffffffffffffffL) || (this.position < (this.end - this.start)))
        {
            int num2;
            if (count == 0)
            {
                return num;
            }
            if ((this.end != 0x7fffffffffffffffL) && (count > ((this.end - this.start) - this.position)))
            {
                count = (int) ((this.end - this.start) - this.position);
            }
            do
            {
                num2 = this.baseStorage.Read(this.start + this.position, buffer, offset, count);
                count -= num2;
                offset += num2;
                this.position += num2;
                num += num2;
            }
            while ((count != 0) && (num2 != 0));
        }
        return num;
    }

    public override long Seek(long offset, SeekOrigin origin)
    {
        this.ThrowIfDisposed();
        switch (origin)
        {
            case SeekOrigin.Begin:
                break;

            case SeekOrigin.Current:
                offset += this.position;
                break;

            case SeekOrigin.End:
                offset += this.Length;
                break;

            default:
                throw new ArgumentException("Invalid Origin enumeration value", "origin");
        }
        if (offset < 0L)
        {
            throw new ArgumentOutOfRangeException("offset", "CannotSeekBeforeBeginning");
        }
        this.position = offset;
        return this.position;
    }

    public override void SetLength(long value)
    {
        throw new NotSupportedException();
    }

    private void ThrowIfDisposed()
    {
        if (this.disposed)
        {
            throw new ObjectDisposedException("StreamOnReadableDataStorage");
        }
    }

    public override void Write(byte[] buffer, int offset, int count)
    {
        throw new NotSupportedException();
    }

    // Properties
    public override bool CanRead
    {
        get
        {
            return !this.disposed;
        }
    }

    public override bool CanSeek
    {
        get
        {
            return !this.disposed;
        }
    }

    public override bool CanWrite
    {
        get
        {
            return false;
        }
    }

    public override long End
    {
        get
        {
            this.ThrowIfDisposed();
            return this.end;
        }
    }

    public override long Length
    {
        get
        {
            this.ThrowIfDisposed();
            if (this.end != 0x7fffffffffffffffL)
            {
                return (this.end - this.start);
            }
            return (this.baseStorage.Length - this.start);
        }
    }

    public override long Position
    {
        get
        {
            this.ThrowIfDisposed();
            return this.position;
        }
        set
        {
            this.ThrowIfDisposed();
            if (value < 0L)
            {
                throw new ArgumentOutOfRangeException("value", "CannotSeekBeforeBeginning");
            }
            this.position = value;
        }
    }

    public override long Start
    {
        get
        {
            this.ThrowIfDisposed();
            return this.start;
        }
    }

    public override DataStorage Storage
    {
        get
        {
            this.ThrowIfDisposed();
            return this.baseStorage;
        }
    }
}


}
