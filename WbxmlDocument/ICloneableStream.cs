﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace WbxmlDocument
{
    internal interface ICloneableStream
    {
        // Methods
        Stream Clone();
    }

 
}
